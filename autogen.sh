#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
#srcdir="$srcdir/src"
test -z "$srcdir" 
#&& srcdir=./src

PKG_NAME="Crescendo"

#(test -f ./configure.in \
#  && test -f $srcdir/crescendo.h) || {

(test -f ./configure.in ) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

. ./macros/autogen.sh
