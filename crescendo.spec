# Note that this is NOT a relocatable package
%define ver      1.1.7
%define rel      1
%define prefix   /usr

Summary: A GNOME interface to the TinyFugue Mud client.
Name: crescendo
Version: %ver
Release: %rel
Copyright: GPL
Group: X11/Applications
Source: ftp://crescendo.edenproject.org/crescendo-%{ver}.tar.gz
BuildRoot: /var/tmp/crescendo-root
URL: http://crescendo.edenproject.org/
Docdir: %{prefix}/doc

%description
Crescendo is a GNOME interface to the TinyFugue Mud client. It
features full ansi color support, a world dialog, split screen
scrollback, and full support for all existing tinyfugue macros
and trigger functions.

If you have any problems, please do not hesitate to contact
Will LaShell <will@lashell.net>

%changelog

* Fri Feb 26 1999 Will LaShell <wlashell@cland.net>

- First 1.0 release.

%prep
%setup

%build
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
make

%install
rm -rf $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)

%doc README AUTHORS ChangeLog NEWS
%{prefix}/bin/crescendo
%{prefix}/share/crescendo/*
%{prefix}/share/gnome/apps/Internet/crescendo.desktop
%{prefix}/share/pixmaps/crescendo.png
