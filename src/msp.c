#include <config.h>
#include <gnome.h>
#include <esd.h>
#include <sys/stat.h>
#include "crescendo.h"
#include "kirsten.h"
#include "msp.h"

/* FIXME: This file won't compile without esound installed */

static void sound_trigger_cb (gchar *trigger, 
							  InputTriggerMethodData *data);
static void play_sound (gchar *filename, gint vol);
static gchar *get_sound_path (gchar *filename);

void
cr_msp_init (void)
{
	cr_input_register_trigger ("!!SOUND", sound_trigger_cb);
}

void
sound_trigger_cb (gchar *trigger, 
				  InputTriggerMethodData *data)
{
	gchar *token;
	gchar *filename;
	gint vol = 100;
	
	token = cr_input_next_token (data, TRUE);
	if (!strchr (token, '(')) {
		g_warning ("Incorrect !!SOUND syntax!");
		return;
	}
	
	token = cr_input_next_token (data, TRUE);
	filename = g_strdup (token);
	
	if (strcmp(&filename[strlen(filename)-4], ".wav") != 0)
	  filename = g_strdup_printf("%s.wav", filename);
	
	/* This should be whitespace */
	token = cr_input_next_token (data, TRUE);

	while (!strchr (token, ')')) {
		token = cr_input_next_token (data, TRUE);
		switch (token[0]) {
		case 'V' :
			sscanf (token + 2, "%d", &vol);
			break;
		case 'L' :
			g_print ("Repeats     : %s", token + 2);
			break;
		case 'P' :
			g_print ("Priority    : %s", token + 2);
			break;
		case 'T' :
			g_print ("Type        : %s", token + 2);
			break;
		case 'U' :
			g_print ("URL         : %s", token + 2);
			break;
		}
		
	}

	play_sound (filename, vol);

	g_free (filename);
}

void 
play_sound (gchar *filename, gint vol)
{
	int sample;
	gchar *sound_path;

	sound_path = get_sound_path (filename);

	if (sound_path == NULL) {
		g_print ("Unable to load sound file.\n");
	} else {
		sample = gnome_sound_sample_load (filename, sound_path);
		esd_set_default_sample_pan (gnome_sound_connection, sample, vol, vol);
		esd_sample_play (gnome_sound_connection, sample);
		esd_sample_free (gnome_sound_connection, sample);
		
		g_free (sound_path);
	}
}

gchar *
get_sound_path (gchar *filename)
{
	/* FIXME: Search ~/.crescendo/sounds/ */
	gchar *sound_path;
	const gchar *home_dir;
	struct stat status;

	home_dir = g_get_home_dir ();
	sound_path = g_strconcat (home_dir, "/.crescendo/sounds/", filename, NULL);
	if (stat (sound_path, &status) == -1)
		g_free (sound_path);
	else
		return sound_path;
	
	sound_path = g_strconcat (CRESCENDO_DATADIR, "sounds/", filename, NULL);
	if (stat (sound_path, &status) == -1)
		g_free (sound_path);
	else 
		return sound_path;
	
	return NULL;
}


