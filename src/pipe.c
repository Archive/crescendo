/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include "crescendo.h"
#include "pipe.h"

int pipes[2]; 

int create_pipe(void) {

	/*
	char *plist[2] = { "/usr/local/bin/tf", NULL };
	*/
	char *plist[2] = { "tf", NULL };
	int pid;
	int pipein[2], pipeout[2];
	
	
	/*pipe(pipein);  
	*/
	
	if(pipe(pipein) == -1){
		fprintf(stderr, "Error: %d\n", errno );
		exit(errno);
	}
	

	
	/* pipe(pipeout);
	*/
	
	
	if(pipe(pipeout) == -1){
		fprintf(stderr, "Error: %d\n", errno );
		exit(errno);
	}
	
	
	pipes[0] = pipeout[0];
	pipes[1] = pipein[1];
	pid = fork();
	if(pid == 0){
		close(0);
		dup(pipein[0]);
		close(1);
		dup(pipeout[1]);

		/*This is where it should execute tf */

		close(pipein[1]); close(pipeout[0]);
		/*
		execv(tfbin, plist);
		*/
		execvp("tf", plist);
		
		_exit(0);  /* This shouldn't be called unless there's a problem. */
	}
	close(pipein[0]); close(pipeout[1]);

	fcntl (pipes[0], F_SETFL, O_NONBLOCK);
	
	return FALSE;
}



