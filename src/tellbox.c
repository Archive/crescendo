/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>

#include "crescendo.h"
#include "standard.h"
#include "cr_functions.h"

#include "tellbox.h"

GtkWidget *boxMsg;
GtkWidget *txtOutput;
GtkWidget *cmbStupid;
	GtkWidget *txtTarget;
GtkWidget *menu, *option_menu;
GtkWidget *item;

gchar *sendstyle;
gchar *text;
	
	gchar *items [] = { N_("tell"), N_("msg"), N_("page") };
	gint i;

static void tbsend (GtkWidget *obj, gpointer data);

void open_tellbox()
{
	GtkWidget *winTellbox;
	GtkWidget *boxMain;
	
	GtkWidget *boxOutput;
	GtkWidget *boxTarget;
	
	winTellbox = gnome_dialog_new("Tell Box",
				      GNOME_STOCK_BUTTON_CLOSE,
				      NULL);
	/* since we only have one button so any "clicked" invocation
	   will just act as close */
	gtk_signal_connect_object(GTK_OBJECT(winTellbox),"clicked",
				  GTK_SIGNAL_FUNC(gnome_dialog_close),
				  GTK_OBJECT(winTellbox));
	gnome_dialog_close_hides(GNOME_DIALOG(winTellbox),FALSE);

	boxMain = GNOME_DIALOG(winTellbox)->vbox;
	
	boxMsg = zvt_term_new();
	gtk_widget_set_usize(boxMsg, 300, 180);
	gtk_box_pack_start(GTK_BOX(boxMain), boxMsg, TRUE, TRUE, 0);

	boxTarget = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(boxMain), boxTarget, FALSE, FALSE, 0);

	cmbStupid = gtk_option_menu_new ();
	menu = gtk_menu_new ();
	for (i = 0; i < 3; i++){

		/* GtkWidget *item;  */

		item = gtk_menu_item_new_with_label (_(items [i]));
		gtk_menu_append (GTK_MENU (menu), item);
		gtk_widget_show(item);
	}
	
	gtk_option_menu_set_menu (GTK_OPTION_MENU (cmbStupid), menu); 
	gtk_box_pack_start(GTK_BOX(boxTarget), cmbStupid, FALSE, FALSE, 0);
	
        txtTarget = gtk_entry_new();
        gtk_box_pack_start(GTK_BOX(boxTarget), txtTarget, TRUE, TRUE, 0);

	boxOutput = gtk_hbox_new(FALSE, 0);	                                        
	gtk_box_pack_start(GTK_BOX(boxMain), boxOutput, FALSE, FALSE, 0);
	
	txtOutput = gtk_entry_new();
	gtk_editable_set_editable(GTK_EDITABLE(txtOutput), TRUE);
	gtk_box_pack_start(GTK_BOX(boxOutput), txtOutput, TRUE, TRUE, 0);
	gtk_signal_connect(GTK_OBJECT(txtOutput), "activate", GTK_SIGNAL_FUNC(tbsend), FROM_RETURN);
	/* gtk_signal_connect(GTK_OBJECT(txtOutput), "changed", GTK_SIGNAL_FUNC(input_push), FROM_CHANGED); */

	gtk_widget_show_all(winTellbox);
}

static void tbsend (GtkWidget *obj, gpointer data)
{
	gint len, target_len;
	gchar *input, *target;
	
	input = gtk_editable_get_chars(GTK_EDITABLE(txtOutput), 0, -1);
	gtk_editable_delete_text(GTK_EDITABLE(txtOutput), 0, -1);
	gtk_editable_set_position(GTK_EDITABLE(txtOutput), 0);
	
	len = strlen(input);
	
	zvt_term_feed(ZVT_TERM(boxMsg), input, len);
	zvt_term_feed(ZVT_TERM(boxMsg), "\r\n", 2);
	
	target = gtk_editable_get_chars(GTK_EDITABLE(txtTarget), 0, -1);
	target_len = strlen(target);
	
	input_push( "tell ", 5);
	
	input_push( target, target_len);
	input_push( " ", 1);
	
	input_push( input, len);
	input_push( "\n", 1);
	
}
