/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>

#include "crescendo.h"

#include "setup.h"
#include "cr_functions.h"
#include "cr-setup-2.xpm"

static void put_config (void);
static void get_config (void);
static void set_config (void);
static void save_config (void);

GtkWidget *chkSplash;
GtkWidget *chkButtonbar;
GtkWidget *chkSplitScroll;
GtkWidget *chkTrans;
GtkWidget *chkShaded;
GtkWidget *chkWstart;
GtkWidget *chkSound;
GtkWidget *chkCmdline;

static cresPrefs *cPrefs;

static void font_sel_ok (GtkWidget	*w, GtkWidget *fsel) {
	gtk_entry_set_text(GTK_ENTRY(cPrefs->cresFont),
		gtk_font_selection_dialog_get_font_name (GTK_FONT_SELECTION_DIALOG(fsel)));

	gtk_widget_destroy (fsel);
}

static void font_sel_cancel (GtkWidget *w, GtkWidget *fsel) {
	gtk_widget_destroy (fsel);
}

static void font_sel(void){

       GtkWidget *fs;
        
        fs = gtk_font_selection_dialog_new("Font");
        gtk_font_selection_dialog_set_font_name(GTK_FONT_SELECTION_DIALOG(fs), 
        gtk_entry_get_text(GTK_ENTRY(cPrefs->cresFont)));

	
        gtk_signal_connect(GTK_OBJECT(GTK_FONT_SELECTION_DIALOG(fs)->ok_button), "clicked",
                GTK_SIGNAL_FUNC(font_sel_ok), fs);
        gtk_signal_connect(GTK_OBJECT(GTK_FONT_SELECTION_DIALOG(fs)->cancel_button), "clicked",
                GTK_SIGNAL_FUNC(font_sel_cancel), fs);

        gtk_widget_show(fs);
}

void show_window_WinSetup (void) 
{
	if(!GTK_WIDGET_VISIBLE(WinSetup)) {
		gtk_widget_show_all(WinSetup);
		put_config ();
		gnome_property_box_set_modified(GNOME_PROPERTY_BOX(WinSetup),FALSE);
	/* only raise if shown and with an X window */
	} else if(WinSetup->window) {
		gdk_window_raise(WinSetup->window);
	}
}


void crescendo_set_font (ZvtTerm *term, char *font_name)
{
	char *s;
	GdkFont *font;
	font = gdk_font_load (font_name);
	zvt_term_set_fonts  (term, font, font);
	if (font)
		zvt_term_set_fonts  (term, font, font);
		s = gtk_object_get_user_data (GTK_OBJECT (term));
        		if (s)
               g_free (s);
	gtk_object_set_user_data (GTK_OBJECT (term), g_strdup (font_name));
	zvt_term_set_font_name(ZVT_TERM(term), font_name);
	cresc_pref->trmfont = font_name;
}

void update_terminal (void)
{
	if (cresc_pref->transparency == 1) {
		gint flags = 0;
		if (cresc_pref->shaded)
			flags = ZVT_BACKGROUND_SHADED;
		zvt_term_set_background(ZVT_TERM(trmOutput), 0, TRUE, 
					flags);
	} else {
		zvt_term_set_background(ZVT_TERM(trmOutput), 0, FALSE, 0);
	}
	
}

static void
apply_cb(GtkWidget *w, gint page, gpointer data)
{
	if(page!=-1)
		return;
	get_config();
	set_config();
	save_config();
	gnome_config_sync();
}

static void trans_cb (GtkWidget *widget, gpointer data)
{
	if (GTK_TOGGLE_BUTTON(widget)->active){
		gtk_widget_set_sensitive (chkShaded, TRUE);
	}
	else {
		gtk_widget_set_sensitive (chkShaded, FALSE);
	}
}


void put_config (void)
{
	if (cresc_pref->transparency==1){
		gtk_widget_set_sensitive(chkShaded, TRUE);
	}
	else {
		gtk_widget_set_sensitive(chkShaded, FALSE);
	}

	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkWstart),
				      cresc_pref->worldstart);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkTrans),
				      cresc_pref->transparency);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkShaded),
				      cresc_pref->shaded);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkSplash),
				      cresc_pref->splash);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkSound),
				      cresc_pref->sound);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkButtonbar),
				      cresc_pref->buttonbar);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkCmdline),
				      cresc_pref->cmdline);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (chkSplitScroll),
				      cresc_pref->splitscroll);
	gtk_entry_set_text(GTK_ENTRY(cPrefs->cresFont), cresc_pref->trmfont);
}
	

/* reads the configuration from the dialog */	
void get_config () 
{
	gchar *newfont;

	cresc_pref->worldstart = GTK_TOGGLE_BUTTON (chkWstart)->active;
	cresc_pref->transparency = GTK_TOGGLE_BUTTON (chkTrans)->active;
	cresc_pref->shaded = GTK_TOGGLE_BUTTON (chkShaded)->active;
	cresc_pref->splash = GTK_TOGGLE_BUTTON (chkSplash)->active;
	cresc_pref->sound = GTK_TOGGLE_BUTTON (chkSound)->active;
	cresc_pref->buttonbar = GTK_TOGGLE_BUTTON (chkButtonbar)->active;
	cresc_pref->cmdline = GTK_TOGGLE_BUTTON (chkCmdline)->active;
	cresc_pref->splitscroll = GTK_TOGGLE_BUTTON (chkSplitScroll)->active;

	
	newfont = g_strdup (gtk_entry_get_text(GTK_ENTRY(cPrefs->cresFont)));
	if (newfont) {
		if (cresc_pref->trmfont != NULL) 
			g_free (cresc_pref->trmfont);
		cresc_pref->trmfont = newfont;
	}

}

/*Put all of the things we want to save from the preferences */
/*Dialog here in this function. */
void save_config (void) 
{
	gnome_config_push_prefix("/Crescendo/");
	gnome_config_set_string("Setup/trmfont", cresc_pref->trmfont);
	gnome_config_set_int("Setup/splash", cresc_pref->splash);
	gnome_config_set_int("Setup/buttonbar", cresc_pref->buttonbar);
	gnome_config_set_int("Setup/cmdline", cresc_pref->cmdline);
	gnome_config_set_int("Setup/splitscroll", cresc_pref->splitscroll);
	gnome_config_set_string("Setup/startsound", cresc_pref->startsound);
	gnome_config_set_int("Setup/sound", cresc_pref->sound);
	gnome_config_set_int("Setup/transparency", cresc_pref->transparency);
	gnome_config_set_int("Setup/shaded", cresc_pref->shaded);
	gnome_config_set_int("Setup/worldstart", cresc_pref->worldstart);
}

/* update the program to reflect the new configuration */
void set_config (void) 
{
	update_terminal ();
	crescendo_set_font (ZVT_TERM (trmOutput), cresc_pref->trmfont);
}


static void
setup_changed_cb(void)
{
	gnome_property_box_set_modified(GNOME_PROPERTY_BOX(WinSetup),TRUE);
}

	

void setup_window_create (void) 
{	
	GtkWidget *w;
	
	GtkWidget *hbox;
	GtkWidget *box;
	GtkWidget *boxFont;
	GtkWidget *button;
	
	cPrefs = malloc(sizeof(cresPrefs));
	
	WinSetup = gnome_property_box_new();
	gtk_window_set_title (GTK_WINDOW(WinSetup),
			      "Crescendo Properties");
	gtk_widget_realize(WinSetup);
	gnome_dialog_close_hides(GNOME_DIALOG(WinSetup),TRUE);
	gtk_signal_connect(GTK_OBJECT(WinSetup),"apply",
			   GTK_SIGNAL_FUNC(apply_cb),
			   NULL);
	
	
	/* ----------FRAME 1 ---------------------*/
	hbox = gtk_hbox_new (FALSE, 0);

	gnome_property_box_append_page(GNOME_PROPERTY_BOX(WinSetup),
				       hbox,
				       gtk_label_new("Display Settings"));

	w = gnome_pixmap_new_from_xpm_d(cr_setup_2_xpm);
	gtk_box_pack_start(GTK_BOX(hbox), w, FALSE, FALSE, 0);

	box = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), box, FALSE, FALSE, 0);
	
	chkWstart = gtk_check_button_new_with_label ("Display World List on startup.");
	gtk_signal_connect(GTK_OBJECT(chkWstart),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(box), chkWstart, FALSE, FALSE, 0);
	
	chkTrans = gtk_check_button_new_with_label ("Transparent Display (zvt)");
	gtk_signal_connect(GTK_OBJECT(chkTrans),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(box), chkTrans, FALSE, FALSE, 0);
	gtk_signal_connect ( GTK_OBJECT(chkTrans), "toggled", GTK_SIGNAL_FUNC(trans_cb), (gpointer) "toggle");

	chkShaded = gtk_check_button_new_with_label ("Transparency shaded");
	gtk_signal_connect(GTK_OBJECT(chkShaded),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX (box), chkShaded, FALSE, FALSE, 0);


	/* ----------FRAME 2 -----------------------*/
	hbox = gtk_hbox_new (FALSE, 0);

	gnome_property_box_append_page(GNOME_PROPERTY_BOX(WinSetup),
				       hbox,
				       gtk_label_new("Runtime Options"));

	w = gnome_pixmap_new_from_xpm_d(cr_setup_2_xpm);
	gtk_box_pack_start(GTK_BOX(hbox), w, FALSE, FALSE, 0);

	box = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), box, FALSE, FALSE, 0);


	chkSplash = gtk_check_button_new_with_label ("Enable Splash Screen");
	gtk_signal_connect(GTK_OBJECT(chkSplash),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(box), chkSplash, FALSE, FALSE, 0);

	chkSound = gtk_check_button_new_with_label ("Enable Sound Support");
	gtk_signal_connect(GTK_OBJECT(chkSound),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(box), chkSound, FALSE, FALSE, 0);
	
	chkButtonbar = gtk_check_button_new_with_label ("Enable Button Bar");
	gtk_signal_connect(GTK_OBJECT(chkButtonbar),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(box), chkButtonbar, FALSE, FALSE, 0);
	
	chkCmdline = gtk_check_button_new_with_label ("Enable command retention");
	gtk_signal_connect(GTK_OBJECT(chkCmdline),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(box), chkCmdline, FALSE, FALSE, 0);
	
	chkSplitScroll = gtk_check_button_new_with_label ("split screen scrollback (Zmud style)");
	gtk_signal_connect(GTK_OBJECT(chkSplitScroll),"toggled",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(box), chkSplitScroll, FALSE, FALSE, 0);

	/* FONT PAGE */
	hbox = gtk_hbox_new (FALSE, 0);

	gnome_property_box_append_page(GNOME_PROPERTY_BOX(WinSetup),
				       hbox,
				       gtk_label_new("Font"));

	w = gnome_pixmap_new_from_xpm_d(cr_setup_2_xpm);
	gtk_box_pack_start(GTK_BOX(hbox), w, FALSE, FALSE, 0);

	box = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), box, FALSE, FALSE, 0);

	/* WARNING UNKNOWN SHIT HERE!!!!!!!!!!! */
	
	boxFont = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(box), boxFont, FALSE, FALSE, 0);

	gtk_container_border_width (GTK_CONTAINER (boxFont), 10);
		
	
	cPrefs->cresFont = gtk_entry_new ();
	gtk_signal_connect(GTK_OBJECT(cPrefs->cresFont),"changed",
			   GTK_SIGNAL_FUNC(setup_changed_cb),NULL);
	gtk_box_pack_start(GTK_BOX(boxFont), cPrefs->cresFont, TRUE, TRUE, 0);
  
	button = gtk_button_new_with_label ("Select...");
	gtk_signal_connect (GTK_OBJECT(button), "clicked",
   			  GTK_SIGNAL_FUNC(font_sel),
   			  NULL);
  
	gtk_box_pack_start(GTK_BOX(boxFont), button, FALSE, FALSE, 2);
  
	/* END OF WIERD SHIT */
}
