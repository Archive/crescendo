/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __tellbox_h__
#define __tellbox_h__

void open_tellbox(void);
void hide_window_tellbox(GtkWidget *obj, GtkWidget *parent);

#endif
