/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>

#include "crescendo.h"
#include "cr_functions.h"
#include "wrapper.h"
#include "notepad.h"
#include "connection.h"
#include "world.h"
#include "toolbar.h"
#include "crescendo.xpm"

gint current_page = 0;

typedef void (*callback_type) (GtkWidget *widget);
callback_type callbacks[10] = { NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL };

GnomeUIInfo toolbar[] =
{
        GNOMEUIINFO_ITEM_DATA(N_("New Connection"),
			      N_("Connect to new world"),
/* 			      open_connection, */
			      show_window_WinWorld,
			      (gpointer)0,
			      NULL),
	GNOMEUIINFO_ITEM_DATA(N_("listworlds"),
			      N_("list worlds"),
			      cr_listworlds,
			      (gpointer)1,
			      NULL),
	GNOMEUIINFO_ITEM_DATA(N_("Next World"),
			      N_("Next World"),
			      cr_dokey_socketf,
			      (gpointer)2,
			      NULL),
	GNOMEUIINFO_ITEM_DATA(N_("Previous World"),
			      N_("Previous World"),
			      cr_dokey_socketb,
			      (gpointer)3,
			      NULL),
	GNOMEUIINFO_ITEM_DATA(N_("Disconnect"),
			      N_("disconnect from the current world"),
			      cr_disconnect,
			      (gpointer)4,
			      NULL),
	GNOMEUIINFO_ITEM_DATA(N_("View Notepad"),
			      N_("view notepad"),
			      show_window_Notepad,
			      (gpointer)5,
			      NULL),
	GNOMEUIINFO_ITEM_DATA(N_("Exit Crescendo"),
			      N_("Exit Crescendo"),
			      exit_app,
			      (gpointer)6,
			      NULL),
/*			      
	GNOMEUIINFO_ITEM_DATA(NULL,
				NULL,
				NULL,
				(gpointer)7,
				crescendo_xpm),			  
*/				
	GNOMEUIINFO_END
};

void build_tool_bar(GtkWidget *parent)
{
	gnome_app_create_toolbar(GNOME_APP(parent), toolbar);
}
