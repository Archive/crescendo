/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* This is the crescendo  function file. all my comman */
/* functions will be available from here. boy I  */
/* hope the works rigth. */

#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>
#include <zvt/vt.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "crescendo.h"
#include "standard.h"
#include "pipe.h"
#include "search.h"
#include "history.h"
#include "notepad.h"
#include "cr_functions.h"
#include "world.h"

#define MAX_BUF 1024

GtkWidget *entInput;
GtkWidget *txtWorld;

gboolean copying;
gboolean escaped;
gint stupidnumber;
gboolean IHATETHIS;

/* prototypes */
static void init_scrollback(void);
static void del_scrollback(void);

void cr_reset ( void)
{
	input_push("/dokey REDRAW\n", 14);
}

void dest_window (GtkWidget *widget)
{
        gtk_widget_destroy(widget);
}

void delete_event (GtkWidget *widget, GdkEvent *event, gpointer data)
{
        exit_app(widget, data);
}

void exit_app (GtkWidget *widget, gpointer data)
{
	save_Notepad_to_disk();
        input_push("/quit\n", 6);
        gtk_main_quit();
}

void dest_main_window (GtkWidget *widget)
{
  gtk_widget_destroy (WinMain);
}

void input_push(gchar *data, gint len)
{
	write(pipes[1], data, len);
}

void input_parse( GtkWidget *widget, gpointer data )
{
        gchar *input, *temp;
        
        if(data != FROM_RETURN) return; /* don't bother yet */
        input = gtk_editable_get_chars(GTK_EDITABLE(widget), 0, -1);
        if(strcmp(input, "/quit") == 0)
	{
        	exit_app(NULL, NULL);
                /* How did we get here? */
                input_push("/quit\n", 6);
                return;
        }
	else if(strcmp(input, "/t1") == 0)
	{
                init_scrollback();
                return;
        }
	else if(strcmp(input, "/t2") == 0)
	{
                del_scrollback();
                return;
        }
        strcpy(temp = (gchar *)g_malloc(strlen(input)+2),input);
 	strcat(temp, (gchar *)"\n");
        input_push(temp, strlen(temp));
	if ( cresc_pref->cmdline == 0 ) {
     	gtk_editable_delete_text(GTK_EDITABLE(widget), 0, -1);
		gtk_editable_set_position(GTK_EDITABLE(widget), 0);
	}
	if ( cresc_pref->cmdline == 1) {
		gtk_editable_select_region( GTK_EDITABLE(widget), 0, -1);
		gtk_editable_set_position(GTK_EDITABLE(widget), strlen(temp));
		stupidnumber = strlen(temp);		
		
	}
       IHATETHIS = 1;
       temp[strlen(temp) - 1]= '\0';
	   add_to_history(temp);
        g_free(input);
}

void input_read ( gpointer data, gint source, GdkInputCondition condition )
{
	gint length;
	gchar buffer[MAX_BUF+1];
	gchar zvt_buf[(MAX_BUF*2)+1];
	gint i;
	static  gint i3 = 0;

	length = read(pipes[0], buffer, MAX_BUF);

	if(length == 0)
	{
		gdk_input_remove(cresc_pref->inputid);
		return;
	}
  
/* Now to clean up buffer */

	i=0;
	i3=0;
  
	while (i < length)
	{
		if (buffer[i] == '\n')
		{
			zvt_buf[i3] = '\r';
			zvt_buf[i3 + 1] = '\n';
			i3 += 2;
		}
		else
		{
			zvt_buf[i3] = buffer[i];
			i3++;
		}
		i++;
	}
	zvt_buf[i3]=0x00;
	zvt_term_feed(ZVT_TERM(trmOutput), zvt_buf, i3);
}

void startup_app ( int argc, char *argv[] )
{
	copying = TRUE;
  	escaped = FALSE;
  	
	read_config();
	
	find_crescendo_bin();	
	setup_window_create();
	create_window_WinWorld();
	create_history();
	if ( cresc_pref->sound == 1 ) {
		gnome_sound_play(cresc_pref->startsound);
	}
		
}

void size_allocate (GtkWidget *widget)
{
        ZvtTerm *term;
        GnomeApp *app;
        XSizeHints sizehints;

        g_assert (widget != NULL);
        term = ZVT_TERM (widget);

        app = GNOME_APP (gtk_widget_get_toplevel (GTK_WIDGET (term)));
        g_assert (app != NULL);

        sizehints.base_width = 
          (GTK_WIDGET (app)->allocation.width) +
          (GTK_WIDGET (term)->style->klass->xthickness * 2) -
          (GTK_WIDGET (term)->allocation.width);

        sizehints.base_height =
          (GTK_WIDGET (app)->allocation.height) +
          (GTK_WIDGET (term)->style->klass->ythickness * 2) -
          (GTK_WIDGET (term)->allocation.height);

        sizehints.width_inc = term->charwidth;
        sizehints.height_inc = term->charheight;
        sizehints.min_width = sizehints.base_width + sizehints.width_inc;
        sizehints.min_height = sizehints.base_height + sizehints.height_inc;

        sizehints.flags = (PBaseSize|PMinSize|PResizeInc);

        XSetWMNormalHints (GDK_DISPLAY(),
                           GDK_WINDOW_XWINDOW (GTK_WIDGET (app)->window),
                           &sizehints);
        gdk_flush ();

}

void change_pos_handler (GtkWidget *widget)
{
        if (cresc_pref->transparency != 1)
                return;

        gtk_widget_queue_draw (trmOutput);
}


/* read in the crescendo config file */

void read_config (void)
{	
	gnome_config_push_prefix("/Crescendo/");
	cresc_pref->splash = (gint)gnome_config_get_int_with_default("Setup/splash=1", NULL);
	cresc_pref->trmfont = gnome_config_get_string_with_default
	   ("Setup/trmfont=-misc-fixed-medium-r-*-*-*-140-*-*-*-*-*-*",
	    NULL);
	cresc_pref->buttonbar = (gint)gnome_config_get_int_with_default("Setup/buttonbar=1", NULL);
	cresc_pref->cmdline = (gint)gnome_config_get_int_with_default("Setup/cmdline=0", NULL);
	cresc_pref->splitscroll = (gint)gnome_config_get_int_with_default("Setup/splitscroll=0", NULL);
	cresc_pref->startsound = gnome_config_get_string_with_default
		("Setup/startsound=" CRESCENDO_DATADIR "/startup2.wav", NULL);
	cresc_pref->sound = gnome_config_get_int_with_default("Setup/sound=1", NULL);
	cresc_pref->transparency = gnome_config_get_int_with_default("Setup/transparency=0", NULL);
	cresc_pref->shaded = gnome_config_get_int_with_default ("Setup/shaded=0", NULL);
	cresc_pref->worldstart = gnome_config_get_int_with_default ("Setup/worldstart=0", NULL);
}


void build_status_bar(GtkWidget *parent)
{
	GtkWidget *conStatus;
	GtkWidget *lblTimer;
	GtkWidget *txtTimer;
	GtkWidget *lblWorld;

        conStatus = gtk_hbox_new(FALSE, 0);
        gtk_box_pack_start(GTK_BOX (parent), conStatus, TRUE, TRUE, 0);

/* Clock Timer */

        lblTimer = gtk_frame_new("Time: ");
        gtk_box_pack_start(GTK_BOX(conStatus), lblTimer, FALSE, FALSE, 0);
        gtk_container_border_width(GTK_CONTAINER(lblTimer), 2);
        gtk_widget_set_usize(lblTimer, 100, 35 );
        gtk_widget_show(lblTimer);
        txtTimer = gtk_clock_new(GTK_CLOCK_REALTIME);
        gtk_container_add(GTK_CONTAINER(lblTimer), txtTimer);
        gtk_widget_show(txtTimer);
                                                        
/* End Clock Timer */

/* World Printout */

        lblWorld = gtk_frame_new("Site: ");
        gtk_box_pack_start(GTK_BOX(conStatus), lblWorld, FALSE, FALSE, 0);
        gtk_container_border_width(GTK_CONTAINER(lblWorld), 2);
        gtk_widget_set_usize(lblWorld, 200, 30);
        gtk_widget_show(lblWorld);
        txtWorld = gtk_label_new("none");
        gtk_container_add(GTK_CONTAINER(lblWorld), txtWorld);
        gtk_widget_show(txtWorld);
                                                
/* End World Printout */

	gtk_widget_show(conStatus);
}

/* Mucho thanks to Michal Zucchi for this function(i may have tweaked it a bit though) -ebernhar 2/7/99 */
/* since we now may be populating the terminal on-screen with the terminal scrollback,
   we *MUST* ensure the line sizes are the same */
static void zvt_term_duplicate_scrollback(ZvtTerm *original, ZvtTerm *dest)
{
	struct vt_line *wn, *nn, *on;
	int count, i;
	int size;

	/* clear whats there */
	vt_scrollback_set(&dest->vx->vt, 0);
	wn = (struct vt_line *)original->vx->vt.scrollback.head;
	/*
	wn = &origional->vx->vt.scrollback.head;
	*/
	nn = wn->next;
	count = 0;
	while(nn) {
		size = dest->vx->vt.width;	/* saves typing is all */
		on = g_malloc(VT_LINE_SIZE(size));
		on->line = -1;		/* forces update */
		on->width = size;
		on->modcount = wn->modcount;
		if (size<=wn->width) {
			/* just copy as much data as we need/have */
			memcpy(on->data, wn->data, 
		       		size * sizeof(uint32));
		} else {
			/* grow the line if we need to */
			memcpy(on->data, wn->data, 
		       		wn->width * sizeof(uint32));
			memset(on->data+wn->width, on->data[wn->width-1] & VTATTR_CLEARMASK, size-wn->width);
		}
		vt_list_addtail(&dest->vx->vt.scrollback, (struct vt_listnode *)on);
		wn = nn;
		nn = wn->next;
		count++;
	}
	
	/* work out how many lines to move into the screen display ... */
	if (original->vx->vt.height < count)
		count = original->vx->vt.height;
	if (count>dest->vx->vt.height)
		count=dest->vx->vt.height;

	/* drop off lines off the screen.  this is a lot simpler/not much slower than having
		to complicate the main loop */
	for (i=0;i<count;i++) {
		wn = (struct vt_line *)vt_list_remtail(&dest->vx->vt.lines);
		g_free(wn);
	}

	/* and move lines from the bottom of the scrollback, to the bottom of the screen ... */
	/* it is absolutely essential the same number of lines removed are added */
	for (i=0;i<count;i++) {
		wn = (struct vt_line *)vt_list_remtail(&dest->vx->vt.scrollback);
		vt_list_addhead(&dest->vx->vt.lines, (struct vt_listnode *)wn);
	}

	/* and make sure we fix up the numbers */
	dest->vx->vt.scrollbacklines = original->vx->vt.scrollbacklines - count;
	dest->vx->vt.scrollbackmax   = original->vx->vt.scrollbackmax;

	return;
}

static void init_scrollback(void)
{
	extern gushort blu[], red[], grn[];
	int width, height;

	g_return_if_fail(trmScrollback == NULL);
	g_return_if_fail(vscrollbar != NULL);
	g_return_if_fail(vpane != NULL);
	g_return_if_fail(trmOutput != NULL);

	/* Set up trmScrollback to be just like trmOutput */
	trmScrollback = zvt_term_new_with_size(ZVT_TERM(trmOutput)->vx->vt.width, 10);
	zvt_term_set_scrollback(ZVT_TERM(trmScrollback), 2000);
	zvt_term_set_bell(ZVT_TERM(trmScrollback), TRUE);
	zvt_term_set_fonts(ZVT_TERM(trmScrollback),
			ZVT_TERM(trmOutput)->font,
			ZVT_TERM(trmOutput)->font_bold);

	height = GTK_WIDGET(vpane)->allocation.height;
	width = GTK_WIDGET(vpane)->allocation.width;

	gtk_paned_add1(GTK_PANED(vpane), trmScrollback);

	/* Move the vscrollbar over to the new terminal */
	gtk_range_set_adjustment(GTK_RANGE(vscrollbar),
						 ZVT_TERM(trmScrollback)->adjustment);
	gtk_paned_set_position(GTK_PANED(vpane),
					(GTK_PANED(vpane)->max_position +
					 GTK_PANED(vpane)->min_position)/ 2);

	/* NOTE: this is my own function, not a zvt_term function -ebernhar 2/7/99 */
	zvt_term_duplicate_scrollback(ZVT_TERM(trmOutput), ZVT_TERM(trmScrollback));	                         

	gtk_widget_realize(trmScrollback);

	zvt_term_set_color_scheme(ZVT_TERM(trmScrollback), red, grn, blu);

	gtk_widget_show(trmScrollback);

	gtk_widget_set_usize(GTK_WIDGET(vpane), width, height);
}

static void del_scrollback(void)
{
	g_return_if_fail(trmScrollback != NULL);
	g_return_if_fail(vpane != NULL);
	g_return_if_fail(vscrollbar != NULL);
	g_return_if_fail(trmOutput != NULL);
	
	/* Move the vscrollbar back */

	gtk_range_set_adjustment(GTK_RANGE(vscrollbar),
		ZVT_TERM(trmOutput)->adjustment);
	
	/* Get rid of trmScrollback */

	gtk_widget_hide(trmScrollback);
	gtk_widget_destroy(trmScrollback);
	trmScrollback = NULL;
	
	gtk_paned_set_position(GTK_PANED(vpane), 0);
}

static void zvt_term_scroll (ZvtTerm *term, int n)
{
	gfloat new_value = 0;
 	if (n)
        {
      		new_value = term->adjustment->value + (n * term->adjustment->page_size);
	}
	else if (new_value == (term->adjustment->upper - term->adjustment->page_size))
	{
      		return;
	}
	else
	{
		new_value = term->adjustment->upper - term->adjustment->page_size;
	}
  	gtk_adjustment_set_value (
      		term->adjustment,
      		n > 0 ? MIN(new_value,
		term->adjustment->upper- term->adjustment->page_size) :
	MAX(new_value, term->adjustment->lower));
}

void cresc_key_press(GtkWidget *widget, GdkEventKey *event) {
	g_return_if_fail(widget != NULL);

	switch(event->keyval) {
			
		case GDK_KP_Page_Up:
		case GDK_Page_Up:
	
			if(cresc_pref->splitscroll) {
				if(trmScrollback == NULL)
					init_scrollback();
				else
					zvt_term_scroll(ZVT_TERM(trmScrollback), -1);
			} else
				zvt_term_scroll(ZVT_TERM(trmOutput), -1);
			gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "key_press_event");
			break;

		case GDK_KP_Page_Down:
		case GDK_Page_Down:
			if(cresc_pref->splitscroll) {
				zvt_term_scroll(((trmScrollback == NULL) ? 
						 ZVT_TERM(trmOutput) : 
						 ZVT_TERM(trmScrollback)), 1);
				/* Check to see if they are at the bottom 
				   of the scrollback now */
				if(trmScrollback == NULL)
					break;

				if(ZVT_TERM(trmScrollback)->vx->vt.scrollbackoffset  == 0)
					del_scrollback();
			} else
				zvt_term_scroll(ZVT_TERM(trmOutput), 1);
			gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "key_press_event");
			break;
		
		case GDK_Up:
			gtk_editable_delete_text(GTK_EDITABLE(entInput), 0, -1);
			increase_history_index();
			gtk_entry_set_text(GTK_ENTRY(entInput), get_from_history());
			gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "key_press_event");
			break;

		case GDK_Down:
			gtk_editable_delete_text(GTK_EDITABLE(entInput), 0, -1);
			decrease_history_index();
			gtk_entry_set_text(GTK_ENTRY(entInput), get_from_history());
			gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "key_press_event");
			break;

		default:
			break;
	}

	if((event->state & (GDK_CONTROL_MASK|GDK_MOD1_MASK)) == 0 &&
	    event->keyval != GDK_Control_L &&
	    event->keyval != GDK_Control_L &&
	    event->keyval != GDK_Meta_L    &&
	    event->keyval != GDK_Meta_L    &&
	    event->keyval != GDK_Alt_L     &&
	    event->keyval != GDK_Alt_R)
	{
		gtk_widget_grab_focus(entInput);        
	}
	return;
}
