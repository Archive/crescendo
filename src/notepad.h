/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __notepad_h__
#define __notepad_h__

void show_window_Notepad(void);
void save_Notepad_to_disk(void);

#endif
