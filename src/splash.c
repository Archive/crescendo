/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include "world.h"
#include "splash.h"
#include "crescendo.h"
#include "cr_functions.h"

GtkWidget *WinSplash;

static void destroy_splash()
{
  gtk_widget_show(WinMain);
	if (cresc_pref->worldstart == 1) 
		{
			show_window_WinWorld();
		}	
}

void display_splash(void){
	
        gnome_config_push_prefix( "/Crescendo/Splash" );
        if ( gnome_config_get_int_with_default("display-splash=1", NULL) )
	{
		GtkWidget *picSplash;
		gchar 	*temp;
		WinSplash = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_signal_connect (GTK_OBJECT (WinSplash), "destroy", GTK_SIGNAL_FUNC (destroy_splash), NULL);
		
		gtk_widget_set_events (WinSplash, GDK_BUTTON_PRESS_MASK);
		gtk_signal_connect (GTK_OBJECT(WinSplash), "button_press_event", GTK_SIGNAL_FUNC (gtk_widget_destroy), NULL);
		
		if(cresc_pref->prefix) {
			temp = g_strconcat (cresc_pref->prefix, "share/crescendo/cr-splash1.jpg", NULL);
			picSplash = gnome_pixmap_new_from_file(temp);
			gtk_container_add(GTK_CONTAINER(WinSplash),picSplash);
			g_free(temp);
		}
		
		gtk_window_position(GTK_WINDOW(WinSplash), GTK_WIN_POS_CENTER);
		gtk_widget_show_all(WinSplash); 
      
	}
	else
	        gtk_widget_show(WinMain);

	gnome_config_pop_prefix();
	gnome_config_sync();
}

void show_splash (void) {
	gtk_widget_show(WinSplash);
}
	


