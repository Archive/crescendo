/* -*- Mode: C; tab-width: 8; indent-tabs-mode: f; c-basic-offset: 8 -*- */

#ifndef __kirsten_h__
#define __kirsten_h__

typedef struct _InputTriggerMethodData  InputTriggerMethodData;

typedef void (*InputTriggerCallback) (gchar *trigger, 
				      InputTriggerMethodData *data);

void cr_input_register_trigger (const char *trigger_text,
				InputTriggerCallback callback);
void cr_input_unregister_trigger (const char *trigger_text);
gchar *
cr_input_next_token (InputTriggerMethodData *data, gboolean return_whitespace);

void cr_input_read (gpointer data, gint source, GdkInputCondition condition);

#endif
