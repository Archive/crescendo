/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include "world.h"
#include "wrapper.h"

static gint worldrow;
static gchar *worldtext;
static gchar *worldtypetext;
static gchar *worldTempText;
static gint len;
static GtkWidget *gclWorldList;
static GtkWidget *newworld;
static GtkWidget *worldname;
static GtkWidget *address;
static GtkWidget *port;
static GtkWidget *username;
static GtkWidget *password;
static GtkWidget *lblStuff;
static GtkWidget *editworld;
static GtkWidget *worldtype;
static GtkWidget *worldtype_menu;

static void save_world_config (GtkCList *clist);
static void read_world_config (GtkCList *clist);
static void cb_menu_select(GtkWidget *item, gchar *type);

static void connect_cb(void)
{
	gchar WorldName[255];
	gchar WAddress[255];
	gchar Username[255];
	gchar Password[255];
	gchar Type[255];

	gtk_clist_get_text(GTK_CLIST(gclWorldList), worldrow, 0, &worldTempText);
	strcpy(WorldName, worldTempText);
	gtk_clist_get_text(GTK_CLIST(gclWorldList), worldrow, 1, &worldTempText);
	strcpy(WAddress, worldTempText);
	gtk_clist_get_text(GTK_CLIST(gclWorldList), worldrow, 2, &worldTempText);
	strcpy(Type, worldTempText);
	gtk_clist_get_text(GTK_CLIST(gclWorldList), worldrow, 3, &worldTempText);
	strcpy(Username, worldTempText);
	gtk_clist_get_text(GTK_CLIST(gclWorldList), worldrow, 4, &worldTempText);
	strcpy(Password, worldTempText);
	cr_connect_withinfo(WorldName, WAddress, Type, Username, Password);
	
	save_world_config(GTK_CLIST(gclWorldList));
	gnome_dialog_close(GNOME_DIALOG(WinWorld));
}

static gint button_press_cb (GtkWidget *widget, 
			     GdkEventButton *event)
{
	if (!gtk_clist_get_selection_info (GTK_CLIST (gclWorldList),
					   (gint) event->x,
					   (gint) event->y,
					   NULL, NULL)) {
		return TRUE;
	}
	
	if (event->type == GDK_2BUTTON_PRESS) {
		  connect_cb();  
	}
	
	return TRUE;
}

void show_window_WinWorld (void)
{
	gtk_widget_show_all(WinWorld);
}

static void selection_made( GtkWidget      *clist,
                          gint            row,
                          gint            column,
                          GdkEventButton *event,
                          gpointer        data )
{
         

         /* Get the text that is stored in the selected row and column
          * which was clicked in. We will receive it as a pointer in the
          * argument text.
          */
         gtk_clist_get_text(GTK_CLIST(clist), row, 1, &worldtext);

         /* Just prints some information about the selected row */
         /*g_print("You selected row %d. More specifically you clicked in "
                 "column %d, and the text in this cell is %s\n\n",
                 row, column, worldtext); */
	 
	 len = strlen(worldtext);
	 worldrow = row;
}

static void read_world_config (GtkCList *clist)
{
	int count;
	int i;

	count = gnome_config_get_int("Worlds/worldcount=0");
	if(count == 0) {
		char *world[5];
 		world[0]="Darkland"; world[1]="darkland.silverden.com 3000";
		world[2]="(None)"; world[3]=""; world[4]="";
		gtk_clist_append(clist, world);
 		world[0]="Nirvana"; world[1]="nirvana.mudservices.com 3500";
		world[2]="(None)"; world[3]=""; world[4]="";
		gtk_clist_append(clist, world);
 		world[0]="Delriad"; world[1]="delriad.silverden.com 8000";
		world[2]="(None)"; world[3]=""; world[4]="";
		gtk_clist_append(clist, world);
		world[0]="Genesis"; world[1]="genesis.cs.chalmers.se 3011";
		world[2]="lp"; world[3]=""; world[4]="";
		gtk_clist_append(clist, world);
		return;
	}
	for(i=0;i<count;i++) {
		char *world[5];
		char *key;
	       
		key = g_strdup_printf("Worlds/%d-name=unknown",i);
		world[0] = gnome_config_get_string(key);
		g_free(key);

		key = g_strdup_printf("Worlds/%d-address=unknown 8000",i);
		world[1] = gnome_config_get_string(key);
		g_free(key);

		key = g_strdup_printf("Worlds/%d-type=unknown",i);
		world[2] = gnome_config_get_string(key);
		g_free(key);

		key = g_strdup_printf("Worlds/%d-username=",i);
		world[3] = gnome_config_get_string(key);
		g_free(key);

		key = g_strdup_printf("Worlds/%d-password=",i);
		world[4] = gnome_config_get_string(key);
		g_free(key);

		gtk_clist_append(clist, world);
	}
}
	

static void save_world_config (GtkCList *clist)
{
	int i;

	gnome_config_clean_section("Worlds/");

	gnome_config_set_int("Worlds/worldcount",clist->rows);
	for(i=0;i<clist->rows;i++) {
		char *text;
		char *key;
		gchar *add_world_string;


		key = g_strdup_printf("Worlds/%d-type",i);
		text = NULL;
		gtk_clist_get_text(clist,i,2,&text);
		if(!text) text="";
		gnome_config_set_string(key,text);
		g_free(key);
		
		add_world_string = text;
	       
		key = g_strdup_printf("Worlds/%d-name",i);
		text = NULL;
		gtk_clist_get_text(clist,i,0,&text);
		if(!text) text="";
		gnome_config_set_string(key,text);
		g_free(key);

		add_world_string = g_strconcat(add_world_string, " ", text, NULL);


		key = g_strdup_printf("Worlds/%d-username",i);
		text = NULL;
		gtk_clist_get_text(clist,i,3,&text);
		if(!text) text="";
		gnome_config_set_string(key,text);
		g_free(key);
		
		add_world_string = g_strconcat(add_world_string, " ", text, NULL);
		
		key = g_strdup_printf("Worlds/%d-password",i);
		text = NULL;
		gtk_clist_get_text(clist,i,4,&text);
		if(!text) text="";
		gnome_config_set_string(key,text);
		g_free(key);
		
		add_world_string = g_strconcat(add_world_string, " ", text, NULL);

		key = g_strdup_printf("Worlds/%d-address",i);
		text = NULL;
		gtk_clist_get_text(clist,i,1,&text);
		if(!text) text="";
		gnome_config_set_string(key,text);
		g_free(key);
		
		add_world_string = g_strconcat(add_world_string, " ", text, NULL);
		
		input_push("/addworld -T", 12);
		input_push(add_world_string, strlen(add_world_string));
		input_push("\n", 1);
		
		add_world_string = NULL;

	}
	
	gnome_config_sync();
}

static void
newworld_clicked(GnomeDialog *newworld, int button, gpointer data)
{
	 char *world[5];
         switch(button) {
         case 0:
					 world[0]=(gtk_entry_get_text(GTK_ENTRY(worldname)));
					 world[1]=g_strconcat(gtk_entry_get_text(GTK_ENTRY(address)), " ", gtk_entry_get_text(GTK_ENTRY(port)), NULL);
					 world[2]=worldtypetext;
					 world[3]=(gtk_entry_get_text(GTK_ENTRY(username)));
					 world[4]=(gtk_entry_get_text(GTK_ENTRY(password)));
					 gtk_clist_append(GTK_CLIST(gclWorldList), world);		 
		 
         case 1:
                 gnome_dialog_close(newworld);
                 break;
         }
}


static void create_new_world (void)
{
	GtkWidget *hbox;
	GtkWidget *tempmenuitem;

	newworld = gnome_dialog_new(_("Add New World"),
				    _("Add"),
				    _("Cancel"),
				    NULL);
	
	lblStuff = gtk_label_new("Add a new world to Crescendo.");
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(newworld)->vbox), lblStuff,
			   FALSE, FALSE, 0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(newworld)->vbox), hbox,
			   TRUE, TRUE, 0);
  	lblStuff = gtk_label_new("World Name:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	worldname = gtk_entry_new();
	gtk_box_pack_end(GTK_BOX(hbox),worldname,
                    FALSE,FALSE,0);
	
	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(newworld)->vbox), hbox,
			   TRUE, TRUE, 0);

	lblStuff = gtk_label_new("Address:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	address = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox),address,
                    FALSE,FALSE,0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(newworld)->vbox), hbox,
			   TRUE, TRUE, 0);

	lblStuff = gtk_label_new("Port:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);
           
	port = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox), port,
					FALSE,FALSE,0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(newworld)->vbox), hbox,
			   TRUE, TRUE, 0);

	lblStuff = gtk_label_new("Password:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	password = gtk_entry_new();
	gtk_entry_set_visibility(GTK_ENTRY(password), FALSE);	
	gtk_box_pack_start(GTK_BOX(hbox),password,
                    FALSE,FALSE,0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(newworld)->vbox), hbox,
			   TRUE, TRUE, 0);

	lblStuff = gtk_label_new("Username:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	username = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox),username,
                    FALSE,FALSE,0);
	
	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(newworld)->vbox), hbox,
			    TRUE, TRUE, 0);
	
	lblStuff = gtk_label_new("World type:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);
	
	worldtype = gtk_option_menu_new();
	gtk_box_pack_start(GTK_BOX(hbox),worldtype,
                    FALSE,FALSE,0);

	/* Now create the menu for the worldtype */
	worldtype_menu = gtk_menu_new();
	tempmenuitem = gtk_menu_item_new_with_label("lp");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "lp");
               
	tempmenuitem = gtk_menu_item_new_with_label("tiny.mush");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "tiny.mush");
                    
	tempmenuitem = gtk_menu_item_new_with_label("tiny.muck");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "tiny.muck");

	tempmenuitem = gtk_menu_item_new_with_label("tiny");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "tiny");
           
	tempmenuitem = gtk_menu_item_new_with_label("diku");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "diku");

	tempmenuitem = gtk_menu_item_new_with_label("aber");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "aber");
	
	tempmenuitem = gtk_menu_item_new_with_label("lpp");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "lpp");
	
	tempmenuitem = gtk_menu_item_new_with_label("telnet");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "telnet");

	gtk_option_menu_set_menu(GTK_OPTION_MENU(worldtype), worldtype_menu);
	
	gtk_signal_connect(GTK_OBJECT(newworld),"clicked",
                    GTK_SIGNAL_FUNC(newworld_clicked),
                    NULL);
		    		    
	/*show the dialog, note that this is not a modal dialog,
	  so the program doesn't block here, but continues*/
	gtk_widget_show_all(newworld);
}


void cb_menu_select( GtkWidget       *item,
										gchar *type )
{	
	    worldtypetext = type;
}

static void
editworld_clicked(GnomeDialog *editworld, int button, gpointer data)
{
	char *world[5];
	switch(button) 
	 {
		 
		case 0:
		 world[0]=(gtk_entry_get_text(GTK_ENTRY(worldname))); world[1]=(gtk_entry_get_text(GTK_ENTRY(address)));
		 world[2]=worldtypetext; world[3]=(gtk_entry_get_text(GTK_ENTRY(username)));
		 world[4]=(gtk_entry_get_text(GTK_ENTRY(password)));
		 
		 gtk_clist_set_text(GTK_CLIST(gclWorldList), worldrow, 0, world[0]);
		 gtk_clist_set_text(GTK_CLIST(gclWorldList), worldrow, 1, world[1]);
		 gtk_clist_set_text(GTK_CLIST(gclWorldList), worldrow, 2, world[2]);
		 gtk_clist_set_text(GTK_CLIST(gclWorldList), worldrow, 3, world[3]);
		 gtk_clist_set_text(GTK_CLIST(gclWorldList), worldrow, 4, world[4]);
		 
		case 1:
		 gnome_dialog_close(editworld);
		 break;
	 }
}

static void delete_world (void)
{
	gtk_clist_remove( GTK_CLIST(gclWorldList), worldrow );
}

/* This is unused for now, but may be usefull 
 in the future - NH */

/* gchar *strGetWorldType(guint intWorldNum) 
{	
	switch (intWorldNum) 
	 { 
		case 0: return "(None)"; break;
		case 1: return "tiny"; break;
		case 2: return "lp"; break;
		case 3: return "diku"; break;
		case 4: return "aber"; break;
		case 5: return "lpp"; break;
		case 6: return "telnet"; break;
	 }
	return "(None)";
} */


static void edit_world (void)
{
	gchar *name;
	gchar *waddress;
	gchar *type;
	gchar *usrname;
	gchar *pssword;
	guint worldnum;
		
	GtkWidget *hbox;
	GtkWidget *tempmenuitem;

	gtk_clist_get_text( GTK_CLIST(gclWorldList),
		    worldrow,
		    0,
                    &name);

	gtk_clist_get_text( GTK_CLIST(gclWorldList),
		     worldrow,
		     1,
		     &waddress);

	gtk_clist_get_text( GTK_CLIST(gclWorldList),
		     worldrow,
		     2,
		     &type);
/*	strcpy(worldtypetext, type); */
	
	gtk_clist_get_text( GTK_CLIST(gclWorldList),
		     worldrow,
		     3,
		     &usrname);

	gtk_clist_get_text( GTK_CLIST(gclWorldList),
		     worldrow,
		     4,
		     &pssword);

	
	editworld = gnome_dialog_new(_("Edit Existing World"),
				     _("Save World"),
				     _("Cancel"),
				     NULL);
	
  	lblStuff = gtk_label_new("Edit this world.");
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(editworld)->vbox), lblStuff,
			   FALSE, FALSE, 0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(editworld)->vbox), hbox,
			   TRUE, TRUE, 0);
  	lblStuff = gtk_label_new("World Name:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	worldname = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY(worldname), name);
	gtk_box_pack_end(GTK_BOX(hbox),worldname,
                    FALSE,FALSE,0);
	
	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_end(GTK_BOX(GNOME_DIALOG(editworld)->vbox), hbox,
			   TRUE, TRUE, 0);

	lblStuff = gtk_label_new("Address and Port:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	address = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(address), waddress);
	gtk_box_pack_start(GTK_BOX(hbox),address,
                    FALSE,FALSE,0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_end(GTK_BOX(GNOME_DIALOG(editworld)->vbox), hbox,
			   TRUE, TRUE, 0);

	lblStuff = gtk_label_new("Password:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	password = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(password), pssword);
	gtk_entry_set_visibility(GTK_ENTRY(password), FALSE);	
	gtk_box_pack_start(GTK_BOX(hbox),password,
                    FALSE,FALSE,0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_end(GTK_BOX(GNOME_DIALOG(editworld)->vbox), hbox,
			   TRUE, TRUE, 0);

	lblStuff = gtk_label_new("Username:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	username = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(username), usrname);
	gtk_box_pack_start(GTK_BOX(hbox),username,
                    FALSE,FALSE,0);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_end(GTK_BOX(GNOME_DIALOG(editworld)->vbox), hbox,
			    TRUE, TRUE, 0);
	
	lblStuff = gtk_label_new("World type:");

	gtk_box_pack_start(GTK_BOX(hbox),lblStuff,
                    FALSE,FALSE,0);

	worldtype = gtk_option_menu_new();
	gtk_box_pack_start(GTK_BOX(hbox),worldtype,
                    FALSE,FALSE,0);

  /* Now create the menu for the worldtype */
	worldtype_menu = gtk_menu_new();
	tempmenuitem = gtk_menu_item_new_with_label("(None)");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "(None)");

	tempmenuitem = gtk_menu_item_new_with_label("tiny");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "tiny");
	
	tempmenuitem = gtk_menu_item_new_with_label("lp");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "lp");
	
	tempmenuitem = gtk_menu_item_new_with_label("diku");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "diku");

	tempmenuitem = gtk_menu_item_new_with_label("aber");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "aber");
	
	tempmenuitem = gtk_menu_item_new_with_label("lpp");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "lpp");
	
	tempmenuitem = gtk_menu_item_new_with_label("telnet");
	gtk_menu_append(GTK_MENU(worldtype_menu), tempmenuitem);
	gtk_widget_show(tempmenuitem);
	gtk_signal_connect (GTK_OBJECT (tempmenuitem), "activate",
               GTK_SIGNAL_FUNC(cb_menu_select),
               "telnet");

	gtk_option_menu_set_menu(GTK_OPTION_MENU(worldtype), worldtype_menu);
	
	worldnum = 0;
        g_print(type);
        if (strcmp(type, "(None)") == 0) worldnum = 0;
	if (strcmp(type, "tiny") == 0) worldnum = 1;
        if (strcmp(type, "lp") == 0) worldnum = 2;
	if (strcmp(type, "diku") == 0) worldnum = 3;
        if (strcmp(type, "aber") == 0) worldnum = 4;
	if (strcmp(type, "lpp") == 0) worldnum = 5;
        if (strcmp(type, "telnet") == 0) worldnum = 6; 
	
	gtk_option_menu_set_history(GTK_OPTION_MENU(worldtype), worldnum);
	
	gtk_signal_connect(GTK_OBJECT(editworld),"clicked",
                    GTK_SIGNAL_FUNC(editworld_clicked),
                    NULL);
	/* show the dialog, note that this is not a modal dialog,
	 so the program doesn't block here, but continues */
	gtk_widget_show_all(editworld);
}


static void
worlds_dialog_clicked(GtkWidget *w, int button, gpointer data)
{
	switch(button) {
	case 0: connect_cb(); break;
	case 1: create_new_world(); break;
	case 2: edit_world(); break;
	case 3: delete_world(); break;
	default:
		save_world_config(GTK_CLIST(gclWorldList));
		gnome_dialog_close(GNOME_DIALOG(w));
		break;
	}
}

void create_window_WinWorld ( void)
{
	gchar *titles[5] = { "World Name", "Address", "Type", "Username", "Password" };
	GtkWidget *vbox;
	GtkWidget *scrolled_world_window;


	WinWorld = gnome_dialog_new(_("Worlds"),
				    _("Launch!"),
				    _("New World"),
				    _("Edit World"),
				    _("Delete World"),
				    GNOME_STOCK_BUTTON_CLOSE,
				    NULL);
	gnome_dialog_close_hides(GNOME_DIALOG(WinWorld),TRUE);
	gtk_signal_connect(GTK_OBJECT(WinWorld),"clicked",
			   GTK_SIGNAL_FUNC(worlds_dialog_clicked),
			   NULL);

	vbox = GNOME_DIALOG(WinWorld)->vbox;
	
	scrolled_world_window = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_world_window),
                                         GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

        gtk_box_pack_start(GTK_BOX(vbox), scrolled_world_window, TRUE, TRUE, 0);

	gclWorldList = gtk_clist_new_with_titles( 5, titles);
	gtk_widget_set_usize(GTK_WIDGET(scrolled_world_window), 350, 150);
	gtk_clist_set_shadow_type (GTK_CLIST(gclWorldList), GTK_SHADOW_OUT);
	gtk_clist_set_column_width (GTK_CLIST(gclWorldList), 0, 100);
	gtk_clist_set_column_auto_resize(GTK_CLIST(gclWorldList), 1, TRUE);
	gtk_clist_set_column_auto_resize(GTK_CLIST(gclWorldList), 2, TRUE);
	gtk_clist_set_column_auto_resize(GTK_CLIST(gclWorldList), 3, TRUE);
	gtk_clist_set_column_visibility(GTK_CLIST(gclWorldList), 4, FALSE);

	read_world_config(GTK_CLIST(gclWorldList));

   	
	gtk_signal_connect(GTK_OBJECT(gclWorldList), "select_row",
                            GTK_SIGNAL_FUNC(selection_made),
                            NULL);

	gtk_container_add(GTK_CONTAINER(scrolled_world_window), gclWorldList);
	gtk_widget_show (gclWorldList);
	gtk_signal_connect (GTK_OBJECT (gclWorldList), "button_press_event",
			    GTK_SIGNAL_FUNC (button_press_cb),
			    NULL);
}
