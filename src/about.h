/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __about_h__ 
#define __about_h__

void about_window_create(GtkWidget *widget, GdkEvent *event);
void show_about_window (void);
void hide_window_WinAbout (void);

#endif
