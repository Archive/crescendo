/* -*- Mode: C; tab-width: 8; indent-tabs-mode: f; c-basic-offset: 8 -*- */

/* Handles input read in from TinyFugue */

/* FIXME: There will be problems detecting triggers/tokens split between 
 * reads. This should be pretty rare, but should be fixed anyway -dave */

#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>
#include <ctype.h>
#include <errno.h>
#include "crescendo.h"
#include "pipe.h"
#include "kirsten.h"

struct _InputTriggerMethodData 
{
	gchar *next_start;
	/*gchar *return_value;*/
};

/* Defines all whitespace/delimeters */
static gchar *token_delim  = " \t\n\r,()\e[]";

#define IS_DELIMETER(ch) (strchr (token_delim, ch) != NULL)

/* Holds the trigger/callback pairs */
static GHashTable *triggers = NULL;

static void parse_input (gchar *str);
gchar *get_escape_token_end (gchar *start);
static gchar *find_trigger (gchar *start);
gchar *dispatch_trigger (gchar *start);
static gchar *get_token_end (gchar *start);
static gboolean is_trigger (gchar *start, gchar *end);
static void copy_buffer (gchar **dest, gchar *start, gchar *end);
static gchar *read_line (void);

void
cr_input_register_trigger (const gchar *trigger_text,
			   InputTriggerCallback callback)
{	
	if (triggers == NULL) {
		triggers = g_hash_table_new (g_str_hash,
					     g_str_equal);
	}
	
	/* Make sure it isn't already in the list */
	g_assert (!g_hash_table_lookup (triggers, trigger_text));
	
	g_hash_table_insert (triggers,
			     g_strdup (trigger_text),
			     callback);
}

void
cr_input_unregister_trigger (const gchar *trigger_text)
{
	gpointer orig_key;
	
	if (g_hash_table_lookup_extended (triggers,
					  trigger_text,
					  &orig_key,
					  NULL)) {
		g_free (orig_key);
	} else {
		g_assert (FALSE || !"Could not find registered trigger");
	}
}

void 
cr_input_read (gpointer data, gint source, GdkInputCondition condition)
{
	gchar *buffer;

	buffer = read_line ();
	
	parse_input (buffer);
}

gchar *
cr_input_next_token (InputTriggerMethodData *data, gboolean return_whitespace)
{
	static gchar *token = NULL;
	static gchar *buffer;
	gboolean have_token = FALSE;
	gchar *end;

	if (token != NULL) {
		g_free (token);
		token = NULL;
	}
	
	while (!have_token) {
		end = get_token_end (data->next_start);
		
		if (end == NULL) {
			buffer = read_line ();
			data->next_start = buffer;
			continue;
		}	
		
		if (!return_whitespace && IS_DELIMETER (*data->next_start)) {
			data->next_start = end;
			continue;
		}
		have_token = TRUE;
	}

	token = g_strndup (data->next_start, end - data->next_start);
	data->next_start = end;
	return token;
}


void
parse_input (gchar *str)
{
	gchar *substr;
	
	substr = find_trigger (str);
	while (substr != NULL) {
		substr = dispatch_trigger (substr);
		if (substr != NULL) 
			substr = find_trigger (substr);
	}
}

gchar *
find_trigger (gchar *str) 
{
	gchar *start;
	gchar *current;
	gchar *zvt_buf;
	gchar *zvt_work;
	gboolean found_trigger = FALSE;
	
	zvt_buf = g_malloc (2 * (strlen (str)) + 1);
	zvt_work = zvt_buf;

        /* Go until a trigger is found */
	start = str;
	
	do {
		current = get_token_end (start);

		if (current == NULL) {
			found_trigger = FALSE;
			break;
		}
		
		if (is_trigger (start, current)) {
			found_trigger = TRUE;
			break;
		}
		
		copy_buffer (&zvt_work, start, current);
		start = current;
	} while (*current != '\0');
	
	
	if (!found_trigger)
		copy_buffer (&zvt_work, start, current);
	
	*zvt_work = '\0';
	zvt_term_feed (ZVT_TERM (trmOutput), zvt_buf, zvt_work - zvt_buf);
	
	g_free (zvt_buf);
	
	return found_trigger ? start : NULL;
}

gchar *
dispatch_trigger (gchar *start)
{
	gchar *end;
	gchar *trigger;
	gchar *ret;
	InputTriggerCallback cb;
	InputTriggerMethodData *data;
	
	end = get_token_end (start);
	
	trigger = g_strndup (start, end - start);	
	data = g_new0 (InputTriggerMethodData, 1);
	
	cb = (InputTriggerCallback)g_hash_table_lookup (triggers, trigger);
	data->next_start = end;
	
	cb (trigger, data);
	
	ret = data->next_start;
	
	g_free (data);
	g_free (trigger);
	
	return ret;
}

gchar *
get_token_end (gchar *start) 
{
	gchar *current;
	gboolean in_whitespace;
	
	if (*start == '\0') return NULL;
	
	if (*start == '\e') return get_escape_token_end (start);
	
	in_whitespace = IS_DELIMETER (*start);
	
	for (current = start; *current != '\0'; current++) {
		if (*current == '\e') {
			return current;
		}
		
		if (in_whitespace ? 
		    !IS_DELIMETER (*current) : IS_DELIMETER (*current)) {
			return current;
		}	
	}	
	return current;
}

gchar *
get_escape_token_end (gchar *start)
{
	g_assert (*start == '\e');
	
	while (!(islower (*start) || isupper (*start) 
		 || *start == '\'' || *start == '@' || *start == '\0')) {
		start++;
	}
	return ++start;
}

gboolean 
is_trigger (gchar *start, gchar *last)
{
	gchar *trigger;
	gboolean ret;

	if (triggers == NULL) return FALSE;

	trigger = g_strndup (start, last - start);
	
	ret = (g_hash_table_lookup (triggers, 
				    trigger) != NULL);
	g_free (trigger);
	
	return ret;
}


void
copy_buffer (gchar **dest, gchar *start, gchar *end)
{
	gchar *dest_buf;
	dest_buf = *dest;

	while (start != end) {
		if (*start == '\n') {
			*dest_buf++ = '\r';
		}
		
		*dest_buf++ = *start++;
	}
	
	*dest = dest_buf;
}

gchar *
read_line (void)
{
	gint n;
	gchar ch;
	static GString *str = NULL;
	
	if (str != NULL) g_string_free (str, TRUE);
	str = g_string_new ("");
	
	while (1) {
		n = read (pipes[0], &ch, 1);
		if (n < 0) {
			/* FiXME: This might cause a problem.  But I don't
			* think it will. -dave */
			if (errno == EAGAIN) {
				break;
			}
			
			g_error (_("Error reading from Tiny Fugue"));
			return NULL;
		}
		
		g_string_append_c (str, ch);

		if (n == 0) {
			break;
		} else if (ch == '\n')
			break;
		
	}

	return str->str;
}
