/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <config.h>
#include <gnome.h>

#include "crescendo.h"
#include "cr_functions.h"
#include "about.h"

GtkWidget *WinAbout;

void hide_window_WinAbout (void) {
	gtk_widget_hide(WinAbout);
}

void about_window_create (GtkWidget *widget, GdkEvent *event)
{
	GtkWidget *about;
	gchar	  *authors[] =
		  {
		  N_("Will LaShell <will@lashell.net>"),
		  N_(" "),
		  N_("retired developers:"),
		  N_("David Winkler"),
		  N_("Visit the crescendo team at:"),
		  N_("http://crescendo.lyrical.net"),
		  NULL
		  };
	

	about  = gnome_about_new(_("Crescendo"), VERSION,
		"(C) 1999, 2000, 2001 Will LaShell",
		(const char **)authors,
		_("Crescendo(c) is a Graphical Front End for Tiny Fugue."),
		NULL);
	gtk_widget_show(about);	
}

void show_about_window (void) {
	gtk_widget_show(WinAbout);
}

