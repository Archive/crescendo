/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __toolbar_h__
#define __toolbar_h__

void build_tool_bar(GtkWidget *parent);

#endif
