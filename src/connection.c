/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>

#include "crescendo.h"
#include "standard.h"
#include "cr_functions.h"
#include "wrapper.h"
#include "connection.h"

GtkWidget *boxSite;
GtkWidget *boxPort;
GtkWidget *boxExisting;
GtkWidget *winOpen;

void connect_me(GtkWidget *widget, GtkWidget *win);

void hide_window_connection(GtkWidget *arg, GtkWidget *parent)
{
	gtk_widget_hide(parent);
}

void open_connection(void)
{
	
	GtkWidget *boxMain;
	GtkWidget *lblSite;
	GtkWidget *lblPort;
	GtkWidget *lblSpace;
	GtkWidget *boxButtons;
	GtkWidget *vbox;
	GtkWidget *vbox2;
	GtkWidget *frmFrame;
	/*GtkWidget *cmdExisting; */ /*unused*/
	
	
	winOpen = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(winOpen), "Connect");
	gtk_window_position(GTK_WINDOW(winOpen), GTK_WIN_POS_CENTER);
	gtk_widget_realize(winOpen);
	gtk_widget_show(winOpen);
	
	boxMain = gtk_vbox_new(FALSE, 0);
	/* gtk_widget_set_usize(boxMain, 200, 115); */
	gtk_container_add(GTK_CONTAINER(winOpen), boxMain);
	gtk_widget_show(boxMain);
	
	lblSite = gtk_label_new("Hostname:");
	gtk_box_pack_start(GTK_BOX(boxMain), lblSite, FALSE, FALSE, 0);
	gtk_widget_show(lblSite);
	
	boxSite = gtk_entry_new();
	gtk_editable_set_editable(GTK_EDITABLE(boxSite), TRUE);
	gtk_box_pack_start(GTK_BOX(boxMain), boxSite, FALSE, FALSE, 0);
	gtk_widget_show(boxSite);
	                                        
	lblPort = gtk_label_new("Port:");
	gtk_box_pack_start(GTK_BOX(boxMain), lblPort, FALSE, FALSE, 0);
	gtk_widget_show(lblPort);
	                                                                
	boxPort = gtk_entry_new();
	gtk_editable_set_editable(GTK_EDITABLE(boxPort), TRUE);
	gtk_box_pack_start(GTK_BOX(boxMain), boxPort, FALSE, FALSE, 0);
	gtk_widget_show(boxPort);

	lblSpace = gtk_label_new("");
	gtk_box_pack_start(GTK_BOX(boxMain), lblSpace, FALSE, FALSE, 0);
	gtk_widget_show(lblSpace);
	
	boxButtons = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(boxMain), boxButtons, FALSE, FALSE, 0);
	gtk_widget_show(boxButtons);
	
	create_button(winOpen, boxButtons, "Connect", connect_me, 100, 20);
	create_button(winOpen, boxButtons, "Cancel", hide_window_connection, 100, 20);
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(boxMain), vbox, FALSE, FALSE, 0);
	gtk_widget_show(vbox);
	
	frmFrame = gtk_frame_new("Existing TinyFugue World");
	gtk_container_border_width(GTK_CONTAINER(frmFrame), 2);
	gtk_box_pack_start(GTK_BOX(boxMain), frmFrame, FALSE, FALSE, 0);
	gtk_widget_show(frmFrame);
	
	vbox2 = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(frmFrame), vbox2);
	gtk_widget_show(vbox2);
	
	boxExisting = gtk_entry_new();
	gtk_editable_set_editable(GTK_EDITABLE(boxExisting), TRUE);
	gtk_container_add(GTK_CONTAINER(vbox2), boxExisting );
	gtk_widget_show(boxExisting);
	
	create_button(winOpen, vbox2, "Go!", connect_existing, 100, 20);
	
}

/*
void open_connection(void)
{
	GtkWidget *winOpen;
	GtkWidget *boxMain;
	GtkWidget *boxButtons;
	GtkWidget *tblMain;
	
	winOpen = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(winOpen), "Open Connection");
	gtk_window_position(GTK_WINDOW(winOpen), GTK_WIN_POS_CENTER);
	gtk_widget_realize(winOpen);

	tblMain = gtk_table_new(1, 2, TRUE);
	gtk_container_add(GTK_CONTAINER(winOpen), tblMain);
	gtk_widget_show(tblMain);
	
	boxMain = gtk_vbox_new(False, 0);
	gtk_table_attach_defaults(GTK_TABLE(tblMain), boxMain, 0, 1, 0, 1);
	
	display_connections(boxMain);

	boxButtons = gtk_vbox_new(FALSE, 0);
	gtk_table_attach_defaults(GTK_TABLE(tblMain), boxButtons, 1, 2, 0, 1);
	
	display_buttons(GTK_WIDGET(winOpen),boxButtons);
	
	gtk_widget_show(boxButtons);
	gtk_widget_show(boxMain);	
	gtk_widget_show(winOpen);
}

void display_connections(GtkWidget *parent)
{
	GtkWidget *frameAvail;
	GtkWidget *scrollbar;
	GtkWidget *txtBox;
		
	frameAvail = gtk_frame_new("Available Connections");
	gtk_box_pack_start(GTK_BOX(parent), frameAvail, FALSE, FALSE, 0);
	gtk_container_border_width(GTK_CONTAINER(frameAvail), 2);
	gtk_widget_set_usize(frameAvail, 200, 200);
	gtk_widget_show(frameAvail);
	
	txtBox = zvt_term_new();
	gtk_container_add(GTK_CONTAINER(frameAvail), txtBox);
	gtk_widget_show(txtBox);
}

void display_buttons(GtkWidget *win, GtkWidget *parent)
{
	GtkWidget *frameAvail;
	GtkWidget *boxMain;
	
	frameAvail = gtk_frame_new("");
	gtk_box_pack_start(GTK_BOX(parent), frameAvail, FALSE, FALSE, 0);
	gtk_container_border_width(GTK_CONTAINER(frameAvail), 2);
	gtk_widget_set_usize(frameAvail, 200, 200);
	gtk_widget_show(frameAvail);
	
	boxMain = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(frameAvail), boxMain);
	gtk_widget_show(boxMain);
	
	create_button(GTK_WIDGET(win), boxMain, "Open", NULL);
	create_button(GTK_WIDGET(win), boxMain, "Edit", NULL);
	create_button(GTK_WIDGET(win), boxMain, "Delete", NULL);
	create_button(GTK_WIDGET(win), boxMain, "Done", hide_window_connection);
}
*/

void create_button(GtkWidget *win, GtkWidget *parent, char *lbl, void *func, gint len, gint width)
{
	GtkWidget *button;
	
	button = gtk_button_new_with_label(lbl);
	gtk_widget_set_usize(button, len, width);
	gtk_signal_connect(
		GTK_OBJECT(button),
		"clicked",
		GTK_SIGNAL_FUNC(func),
		win);
	gtk_box_pack_start(GTK_BOX(parent), button, FALSE, FALSE, 0);
	gtk_widget_show(button);
}

void connect_me(GtkWidget *widget, GtkWidget *win)
{
        gchar *host, *port;
        int len;
	   int len2;
		
        if(boxSite==NULL) return;
        host = gtk_editable_get_chars(GTK_EDITABLE(boxSite), 0, -1);
        port = gtk_editable_get_chars(GTK_EDITABLE(boxPort), 0, -1);
	   
	   len = strlen(host);
	   len2 = strlen(port);
	   
	   input_push("/world ", 7);
	   input_push(host, len);
	   input_push(" ", 1);
	   input_push(port, len2);
	   input_push("\n", 1);
    
	   
	gtk_label_set(GTK_LABEL(txtWorld), host);
	gtk_widget_hide(win);
}

void connect_existing()
{
	gchar *world;
	gint len;
	
	if(boxExisting == NULL){ printf("DAMMIT\n"); return; }
	
	world = gtk_editable_get_chars(GTK_EDITABLE(boxExisting), 0, -1);
	len = strlen(world);
	
	cr_connect_existing(world, len);
	
	g_free(world);
	gtk_widget_hide(winOpen);
}
