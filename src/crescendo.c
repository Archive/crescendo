/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>
#include <time.h>

#include "crescendo.h"
#include "standard.h"
#include "setup.h"
#include "search.h"
#include "cr_functions.h"
#include "toolbar.h"
#include "color.h"
#include "pipe.h"
#include "world.h"
#include "kirsten.h"
#include "msp.h"
#include "menu.h"

GtkWidget *WinMain;
GtkWidget *txtOutput;
GtkWidget *WinSetup;
GtkWidget *WinWorld;
GtkStyle *Output_Style;
GtkWidget *trmOutput;
GtkWidget *trmScrollback;
GtkWidget *vpane;
GtkWidget *vscrollbar;
GtkWidget *entInput;
CrescPref *cresc_pref;

/*begin mainline. */

int main (int argc, char *argv[])
{
	
	/*Here's where we put all the widget names 	*/
	/* I want to use. 				*/
	
	GtkWidget	*conWinMain;
	GtkWidget	*conOutput;
			
	GtkWidget	*txtStatus;
	GtkWidget	*conInput;
	GtkWidget	*conStatus;

/*	GtkWidget	*pixCrescendo; */
	
	gnome_init ("crescendo", VERSION, argc, argv);
	
	/*FIXME: perhaps cresc_pref should not be a pointer, but
	  the structure itself */
	cresc_pref = g_new0(CrescPref,1);
	
	cresc_pref->prefix = (gchar *)find_crescendo_bin();
	startup_app(0, NULL);
	
/* Display the Splash Screen */
	if ( cresc_pref->splash==1){
		display_splash();
	}
	
	WinMain = gnome_app_new("Crescendo", "Crescendo");

	build_menu_bar_and_status_bar(WinMain);
	
	if ( cresc_pref->buttonbar==1) 
	{
		build_tool_bar(WinMain);
	}

	gtk_widget_realize(WinMain);
		
	conWinMain = gtk_vbox_new(FALSE, 0);

	gtk_signal_connect(GTK_OBJECT(WinMain), "key_press_event",
			GTK_SIGNAL_FUNC(cresc_key_press), NULL);
	
	gnome_app_set_contents(GNOME_APP(WinMain), conWinMain); 
                                
/* Create a Box for Status Bar / Pixmap */

	conStatus = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(conWinMain), conStatus, FALSE, FALSE, 0);

/* Load Status Bar */
	
	build_status_bar(conStatus);

/*This is where i add in the pixmap */
	
	gtk_widget_show(conWinMain);
	
/*	pixCrescendo = gnome_pixmap_new_from_xpm_d(crescendo_xpm);

	gtk_box_pack_end(GTK_BOX(conStatus), pixCrescendo, FALSE, FALSE, 1); */
	gtk_widget_show(conStatus);
	
	/*Here we have the output container/frame. This is 	*/
	/*definately an experiment. :) 					*/
		
	conOutput = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start (GTK_BOX (conWinMain), conOutput, TRUE, TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (conWinMain), 1);
	
	/*Inside the output container we are going to put the	*/
	/* output text box and scroll bar. 				*/
	vpane = gtk_vpaned_new();
	
	gtk_box_pack_start(GTK_BOX(conOutput), vpane, TRUE, TRUE, 0);

	trmOutput = zvt_term_new_with_size(80, 24);
	gtk_paned_add2(GTK_PANED(vpane), trmOutput);
	
	zvt_term_set_scrollback(ZVT_TERM(trmOutput), 2000);
	zvt_term_set_bell(ZVT_TERM(trmOutput), TRUE);
	zvt_term_set_font_name(ZVT_TERM(trmOutput), cresc_pref->trmfont);
	
	update_terminal ();

	gtk_widget_realize(trmOutput);
	gtk_widget_realize(vpane);
	
	zvt_term_set_color_scheme( (ZvtTerm *)trmOutput, red, grn, blu );
	gtk_signal_connect_after (GTK_OBJECT (trmOutput), "size_allocate",
							  GTK_SIGNAL_FUNC (size_allocate), trmOutput);
	gtk_signal_connect (GTK_OBJECT (WinMain), "configure_event",
						GTK_SIGNAL_FUNC (change_pos_handler), NULL);

	vscrollbar = gtk_vscrollbar_new  (GTK_ADJUSTMENT (ZVT_TERM(trmOutput)->adjustment));
	gtk_box_pack_start(GTK_BOX(conOutput), vscrollbar, FALSE, FALSE, 0);
	gtk_widget_show(vscrollbar);
	
	gtk_widget_show(trmOutput);
	
	gtk_widget_show(vpane);
		
	gtk_widget_show(conOutput);
	
	create_pipe();
	
	txtStatus = gtk_text_new (NULL, NULL);
	gtk_widget_set_usize (txtStatus, 500, 25);
	gtk_text_set_editable(GTK_TEXT(txtStatus), FALSE);
	gtk_box_pack_start (GTK_BOX(conWinMain), txtStatus, FALSE, TRUE, 0);
	/*	gtk_widget_show (txtStatus); */
		
	conInput = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start (GTK_BOX (conWinMain), conInput, FALSE, FALSE, 0);
	gtk_container_border_width (GTK_CONTAINER (conWinMain), 1);
	
	entInput = gtk_entry_new();
	gtk_editable_set_editable(GTK_EDITABLE(entInput), TRUE);
	gtk_box_pack_start (GTK_BOX(conInput), entInput, TRUE, TRUE, 0);
	gtk_widget_show (entInput);
	gtk_signal_connect (GTK_OBJECT (entInput), "activate", GTK_SIGNAL_FUNC (input_parse), FROM_RETURN);
	gtk_signal_connect (GTK_OBJECT (entInput), "changed", GTK_SIGNAL_FUNC(input_parse), FROM_CHANGED);
	
	gtk_widget_show (conInput);	
	gtk_window_position(GTK_WINDOW(WinMain), GTK_WIN_POS_CENTER);
	gtk_signal_connect (GTK_OBJECT (WinMain), "destroy", GTK_SIGNAL_FUNC (exit_app), NULL);

	cresc_pref->inputid = gdk_input_add(pipes[0], GDK_INPUT_READ, cr_input_read, txtOutput);
	
	/* Initialize MSP support */
	cr_msp_init ();

	/* now this is where it sits and waits for an event to happen. */

	gtk_widget_realize(WinMain);
/*	gtk_widget_show(pixCrescendo); */
	if ( cresc_pref->splash != 1)
	{
		gtk_widget_show(WinMain);
		if (cresc_pref->worldstart == 1) 
		{
			show_window_WinWorld();
		}	
	}

	/* UGLY HACK UGLY HACK FIX THIS FIX IT FIX IT FIX IT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
	
	input_push("/visual off\n", 12);
	
	/* FIX THIS DAMMIT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
	gtk_main();
	
	return 0;
}

