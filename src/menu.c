/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>

#include "crescendo.h" 
#include "cr_functions.h" 
#include "about.h" 
#include "connection.h" 
#include "tellbox.h"
#include "notepad.h"
#include "menu.h"
#include "world.h"

static GnomeUIInfo viewmenu[] =
{
	{
		GNOME_APP_UI_ITEM,
		N_("_Tell Box"),
		N_("Tell Box"),
		open_tellbox,
		NULL,
		NULL,
		0,
		0,
		0,
		0,
		NULL
	},
	
	{	GNOME_APP_UI_ITEM,
		N_("_Notepad"),
		N_("Notepad"),
		show_window_Notepad,
		NULL,
		NULL,
		0,
		0,
		0,
		0,
		NULL
	},
	
	{	GNOME_APP_UI_ITEM,
		N_("_reset"),
		N_("reset"),
		cr_reset,
		NULL,
		NULL,
		0,
		0,
		0,
		0,
		NULL
	}, 
	
	GNOMEUIINFO_END
};

static GnomeUIInfo filemenu[] =
{
	{
		GNOME_APP_UI_ITEM,
		N_("_Connect"),
		N_("Connect"),
		show_window_WinWorld,
		NULL,
		NULL,
		0,
		0,
		0,
		0,
		NULL
	},
	{
		GNOME_APP_UI_ITEM,
		N_("_Disconnect"),
		N_("Disconnect"),
		NULL,
		NULL,
		NULL,
		0,
		0,
		0,
		0,
		NULL
	},
	{
		GNOME_APP_UI_ITEM,
		N_("_Exit Crescendo"),
		N_("Exit Crescendo"),
		exit_app,
		NULL,
		NULL,
		GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_EXIT,
		0,
		0,
		NULL
	},
	GNOMEUIINFO_END
};

static GnomeUIInfo setupmenu[] =
{
	GNOMEUIINFO_MENU_PREFERENCES_ITEM(show_window_WinSetup,NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo helpmenu[] =
{
	GNOMEUIINFO_MENU_ABOUT_ITEM(about_window_create, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo mainmenu[] =
{
	GNOMEUIINFO_SUBTREE(N_("_World"), filemenu),

	GNOMEUIINFO_MENU_VIEW_TREE(viewmenu),
	
	GNOMEUIINFO_MENU_SETTINGS_TREE(setupmenu),
	GNOMEUIINFO_MENU_HELP_TREE(helpmenu),
	GNOMEUIINFO_END
};

void build_menu_bar_and_status_bar(GtkWidget *app)
{
#if 0
	GtkWidget *appbar;
#endif

	gnome_app_create_menus(GNOME_APP(app), mainmenu);

#if 0
	appbar = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar(GNOME_APP(app), appbar);

	gnome_app_install_menu_hints(GNOME_APP(app), mainmenu);
#endif
}
