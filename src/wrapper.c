#include <config.h>
#include <gnome.h>
#include <zvt/zvtterm.h>

#include "crescendo.h"
#include "cr_functions.h"
#include "wrapper.h"

void feed_tf(gchar *data, gint len)
{
	
	input_push(data, len);
	input_push("\n", 1);	
}

void cr_listworlds(void) 
{
	feed_tf("/listworlds", 11);
}

void cr_dokey_socketf(void)
{
	feed_tf("/dokey socketf", 14);
}

void cr_dokey_socketb()
{
	feed_tf("/dokey socketb", 14);
}

void cr_disconnect()
{
	feed_tf("/dc", 3);
}

void cr_connect_existing(gchar *data, gint len)
{
	input_push("/world ", 7);
	input_push(data, len);
	input_push("\n", 1);
}
 
void cr_connect_withinfo(gchar *worldname, gchar *waddress, gchar *type, gchar *username, gchar *password) {
	if ((strcmp(username, "") == 0) || (strcmp(password, "") == 0))
		 {
			 cr_connect_unlisted(waddress, strlen(waddress));
		 }
			else
		 { 
			 /* Add the world */
			 input_push("/addworld -T", 12);
			 input_push(type, strlen(type));
			 input_push(" ", 1);
			 input_push(worldname, strlen(worldname));
			 input_push(" ", 1);
			 input_push(username, strlen(username));
			 input_push(" ", 1);
			 input_push(password, strlen(password));
			 input_push(" ", 1);
			 input_push(waddress, strlen(waddress));
			 input_push("\n", 1);
			 
			 /* Connect to it */
			 input_push("/connect ", 9);
			 input_push(worldname, strlen(worldname));
			 input_push("\n", 1);
		 }
}

void cr_connect_unlisted(gchar *data, gint len)
{
	input_push("/world ", 7);
	input_push(data, len);
	input_push("\n", 1);
}


/*
 * 
 * void cr_set_alias(gchar)
 * {
 * 	
 * 	input_push"
 * }
 */ 
/*
 * // void input_push(gchar *data, gint len)
 * // 
 * //         write(pipes[1], data, len);
 */ 

