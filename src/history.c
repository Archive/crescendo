/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>

#include "crescendo.h"
#include "history.h"

GList *history_head;
GList *cur_history;

void create_history(void) 
{
	history_head = cur_history = NULL;
}

void add_to_history(gchar *data) 
{
	if (data[0] != '\n') {
		cur_history = NULL;
		history_head = g_list_prepend (history_head, data);
	}
}


void reset_history(void)
{
	cur_history = NULL;
}

gchar* get_from_history(void) 
{
	return (cur_history) ? cur_history->data : "";
}


void decrease_history_index(void)
{
	if (cur_history == NULL)
		cur_history = g_list_last (history_head);
	else
		cur_history = g_list_previous (cur_history);	
}

void increase_history_index(void)
{	
	if (cur_history == NULL)
		cur_history = history_head;
	else
		cur_history = g_list_next (cur_history);
}

	
 



