/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __setup_h__
#define __setup_h__

#include <zvt/zvtterm.h>
#include "crescendo.h"

void app_end_with_save (void);

typedef struct _cresPrefs
{
	GtkWidget *cresFont;
} cresPrefs;

extern GtkWidget *chkSplash;
extern GtkWidget *chkButtonbar;

void setup_window_create (void);
void show_window_WinSetup (void);
void update_terminal (void);
void add_font_page (void);
void crescendo_set_font (ZvtTerm *term, char *font_name);

#endif
