#ifndef __wrapper_h__
#define __wrapper_h__

void feed_tf(gchar *data, gint len);
void cr_listworlds(void);
void cr_dokey_socketf(void);
void cr_dokey_socketb(void);
void cr_disconnect(void);
void cr_connect_withinfo(gchar *worldname, gchar *waddress, gchar *type, gchar *username, gchar *password);
void cr_connect_existing(gchar *data, gint len);
void cr_connect_unlisted(gchar *data, gint len);

#endif
