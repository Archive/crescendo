/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>

#include "crescendo.h"
#include "cr_functions.h"
#include "notepad.h"

static GtkWidget *winNotepad = NULL;
static GtkWidget *txtOutput = NULL;

static int save_timeout = 0;

void
save_Notepad_to_disk(void)
{
	char *filename;
	FILE *fp;
	char *chars;

	if(!txtOutput)
		return;

	if(save_timeout) {
		gtk_timeout_remove(save_timeout);
		save_timeout = 0;
	}

	filename = gnome_util_home_file("Crescendo-notepad");
	fp = fopen(filename,"w");
	g_free(filename);
	if(!fp)
		return;

	chars = gtk_editable_get_chars(GTK_EDITABLE(txtOutput),0,-1);
	fputs(chars,fp);
	fclose(fp);
}

static void
load_Notepad_from_disk(GtkText *text)
{
	char buf[1024];
	char *filename = gnome_util_home_file("Crescendo-notepad");
	FILE *fp;

	fp = fopen(filename,"r");
	g_free(filename);
	if(!fp)
		return;
	gtk_editable_delete_text(GTK_EDITABLE(text),0,-1);

	while(fgets(buf,1024,fp)) {
		gtk_text_insert(text, NULL, NULL, NULL, buf, strlen(buf));
	}
	fclose(fp);
}

static int
save_handler(gpointer data)
{
	save_timeout = 0;
	save_Notepad_to_disk();
	return FALSE;
}

static void
really_clear_notepad(gint reply, gpointer data)
{
	if(reply == 0) {
		GtkEditable *editable = data;
		gtk_editable_delete_text(editable,0,-1);
	}
}

static void
clicked_notepad(GtkWidget *notepad, int button, GtkText *text)
{
	GtkWidget *dlg;
	switch(button) {
	case 0:
		dlg = gnome_ok_cancel_dialog_parented(
				      _("Really clear notepad?"),
				      really_clear_notepad,
				      text,
				      GTK_WINDOW(notepad));
		gtk_window_set_modal(GTK_WINDOW(dlg),TRUE);
		gtk_widget_show(dlg);
		break;
	case 1:
		gnome_dialog_close(GNOME_DIALOG(notepad));
		break;
	}
}

static void
text_changed(void)
{
	if(!save_timeout)
		gtk_timeout_add(10000,save_handler,NULL);
}

static void
create_Notepad(void)
{
	winNotepad = gnome_dialog_new(_("Crescendo Notepad"),
				      _("Clear"),
				      GNOME_STOCK_BUTTON_CLOSE,
				      NULL);
	/* since we only have one button so any "clicked" invocation
	   will just act as close */
	gnome_dialog_close_hides(GNOME_DIALOG(winNotepad),TRUE);

	txtOutput = gtk_text_new(NULL, NULL);
	gtk_widget_set_usize(txtOutput, 200, 260);
	gtk_editable_set_editable(GTK_EDITABLE(txtOutput), TRUE);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(winNotepad)->vbox),
			   txtOutput, TRUE, TRUE, 0);

	gtk_signal_connect(GTK_OBJECT(winNotepad),"clicked",
			   GTK_SIGNAL_FUNC(clicked_notepad),
			   txtOutput);

	load_Notepad_from_disk(GTK_TEXT(txtOutput));

	gtk_signal_connect(GTK_OBJECT(txtOutput),"changed",
			   GTK_SIGNAL_FUNC(text_changed), NULL);
}

void
show_window_Notepad(void)
{
	if(!winNotepad)
		create_Notepad();

	gtk_widget_show_all(winNotepad);
}
