/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __crescendo_h__
#define __crescendo_h__

/* I am putting all the main variable */
/* and other definitions in here. this */
/* file is designed to be included in most */
/* all the other files, as it will contain */
/* the essential crap to make it work. */

extern GtkWidget *WinMain;
extern GtkWidget *txtOutput;
extern GtkWidget *WinSetup;
extern GtkStyle *Output_Style;
extern GtkWidget *trmOutput;
extern GtkWidget *trmScrollback;
extern GtkWidget *vpane;
extern GtkWidget *vscrollbar;
extern GtkWidget *entInput;
extern GtkWidget *winNotepad;
extern GtkWidget *WinWorld;

/* testing the CrescPref struct */

typedef struct _CrescPref CrescPref;
struct _CrescPref
{
	gint inputid;
	gchar *tfbin;
	gint   splash;
	gint   buttonbar;
	gchar *workingdir;
	gchar *datadir;
	gchar *fullpath;
	gchar *prefix;
	gint   cmdline;
	char *trmfont;
	char *crescendo_font;
	gchar *startsound;
	gint	splitscroll;
	gint sound;
	gint transparency;
	gint shaded;
	gint worldstart;
};

extern CrescPref *cresc_pref;

void cresc_key_press(GtkWidget *widget, GdkEventKey *event);
void input_push(gchar *data, int len);

#endif
