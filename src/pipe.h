/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __pipe_h__ 
#define __pipe_h__

#include "crescendo.h"

extern int pipes[2];

int create_pipe(void);

#endif
