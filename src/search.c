/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <sys/stat.h>

#include "crescendo.h"  
#include "search.h"

gchar *find_crescendo_bin (void)
{
        gchar *home;
        gchar **patharray;
	gchar *temp, *temp2;
        gint i, j;
        struct stat status;

	temp = g_strconcat (CRESCENDO_BINDIR, "/crescendo", NULL);
	if (stat (temp, &status) == -1) {
		g_free (temp);
	} else {
		if ((temp2 = strstr (temp, "bin"))) 
			*temp2 = 0;
		
		return temp;
	}


        i = j = 0;
        cresc_pref->fullpath = getenv ("PATH");   
        patharray = g_strsplit (cresc_pref->fullpath, ":", 256);
        for (i = 0; patharray[i]; i++) 
	   {
                /* append patharray[i] to temp */
		
                temp = g_strconcat (patharray[i], "/crescendo", NULL);

                /* Check whether or not temp exists */
                if (stat (temp, &status) == -1)
                {
                        /* If not, free patharray[i] and temp,
                           increment i and contiune */
                        g_free (temp);
                        g_free (patharray[i]);
                   /*     i++; */
                        continue;
                }
                else
                {
                        /* If so, free the rest of the array,
                           and return the full path of the
                           crescendo binary */
		        g_free( temp );
			temp = patharray[i];
			i++;
			while (patharray[i] != NULL)
			{
		                g_free (patharray[i]);
				i++;
			}
			if ( ( temp2 = strstr( temp, "bin" ) ) )
			  *temp2 = 0;
			return temp;
                }
        }

        /* If we get here, it wasn't in the path, so check the
           home directory */
        
        home = getenv ("HOME");
        temp = g_strconcat( home, "/crescendo", NULL );
        if (stat (temp, &status) == -1)
        {
                g_free (temp);
                return NULL;
        }
        else
	{
                g_free (temp);
		temp = strdup( home );
		if ( ( temp2 = strstr( temp, "bin" ) ) )
		  *temp2 = 0;
                return temp;
	}
}



