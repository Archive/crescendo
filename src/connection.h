/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __connection_h__
#define __connection_h__

void open_connection(void);
void display_connections(GtkWidget *parent);
void display_buttons(GtkWidget *win, GtkWidget *parent);
void connect_existing(void);
void hide_window_connection(GtkWidget *arg, GtkWidget *parent);

#endif
