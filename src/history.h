/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __history_h__
#define __history_h__

void create_history(void);
void add_to_history(gchar *data);
void reset_history(void);
gchar* get_from_history(void) ;
void increase_history_index(void);
void decrease_history_index(void);

#endif
