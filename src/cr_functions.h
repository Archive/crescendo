/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __cr_functions_h__ 
#define __cr_functions_h__

extern GtkWidget *entInput;
extern GtkWidget *txtWorld;

void create_button(GtkWidget *win, GtkWidget *parent, char *lbl, void *func, gint len, gint width);
void build_menu_bar(GtkWidget *parent);

/* app shut down function.*/

void delete_event();

/* reading func. */
void input_read();

void cr_reset ( void);
void test_button_push (GtkWidget *widget, gpointer data);
void exit_app (GtkWidget *widget,  gpointer data);
void input_push(gchar *data, int len);
void input_parse( GtkWidget *widget, gpointer data );
void tellbox_output(GtkWidget *widget, gpointer data);
void input_read ( gpointer data, gint source, GdkInputCondition condition );
void delete_event (GtkWidget *widget, GdkEvent *event, gpointer data);
void exit_menu (GtkWindow *window, GtkItemFactory *menubar_factory);
void dest_window (GtkWidget *widget);
void dest_main_window (GtkWidget *widget);
void read_config (void);
void build_button_bar(GtkWidget *parent);
void setup_window_create(void);
void about_window_create(GtkWidget *widget, GdkEvent *event);
void show_window_WinSetup(void);
void build_status_bar(GtkWidget *parent);
void display_splash(void);
void startup_app(int argc, char *argv[] );
void size_allocate (GtkWidget *widget);
void change_pos_handler (GtkWidget *widget);

#endif
