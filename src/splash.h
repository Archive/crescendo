/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __splash_h__
#define __splash_h__

void display_splash (void);
void hide_splash (void);
void show_splash (void);

#endif
