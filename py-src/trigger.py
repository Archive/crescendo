from gtk import *
from gnome.ui import *

import edittrigger
import addtrigger

class trigger(GtkDialog):
	def __init__( self, parent ):
		GtkDialog.__init__( self )
		self.parent = parent
		self.selected_row = None
		self.set_default_size( 670, 250 )
		self.connect("destroy", self.close )
		titles = ["Trigger Name", "Match Pattern", "Pattern Type", "Text To Send" ]
		self.trigger_list = GtkCList( 4, titles )
		self.trigger_list.set_column_width( 0, 150 )
		self.trigger_list.set_column_width( 1, 220 )
		self.trigger_list.set_column_width( 2, 100 )
		self.trigger_list.set_column_width( 3, 200 )
		self.load_triggers( self.trigger_list )
		self.trigger_list.connect( "select_row", self.select_row )
		self.trigger_list.show()
		
		self.vbox.pack_start( self.trigger_list, expand=TRUE, fill=TRUE )
		btn = GtkButton( "Activate" )
		btn.connect( "clicked", self.click_activate )
		btn.show()
		self.action_area.pack_start( btn )
		btn = GtkButton( "Deactivate" )
		btn.connect( "clicked", self.deactivate_trigger )
		btn.show()
		self.action_area.pack_start(btn)
		btn=GtkButton( "Add New" )
		btn.connect( "clicked", self.add_trigger )
		btn.show()
		self.action_area.pack_start( btn )
		btn=GtkButton( "Edit" )
		btn.connect( "clicked", self.edit_trigger )
		btn.show()
		self.action_area.pack_start( btn )
		btn = GtkButton( "Delete" )
		btn.connect( "clicked", self.delete_trigger )
		btn.show()
		self.action_area.pack_start( btn )
		btn = GtkButton( "Cancel" )
		btn.connect( "clicked", self.close )
		btn.show()
		self.action_area.pack_start( btn )
		self.realize()

	def select_row( self, clist, row, col, event ):
		if row == self.selected_row:
			self.activate_trigger( row )
		else:
			self.selected_row = row

	def click_activate( self, row=None ):
		row = self.selected_row
		self.activate_trigger( row )
	

	def activate_trigger( self, row=None ):
		if row == None:
			row = self.selected_row
		triggername = self.trigger_list.get_text( row, 0 )
		matchpattern = self.trigger_list.get_text( row, 1 )
		patterntype = self.trigger_list.get_text( row, 2 )
		texttosend = self.trigger_list.get_text( row, 3 )
		world = self.parent.get_current_world()
		if (world != 0):
			world.get_data( 'stub' ).def_trigger( triggername, matchpattern, patterntype, texttosend )

	def deactivate_trigger( self, event=None ):
		row = self.selected_row
		triggername = self.trigger_list.get_text( row, 0 )
		world = self.parent.get_current_world()
		if (world != 0):
			world.get_data( 'stub' ).undef_trigger( triggername )

	def add_trigger( self, event=None ):
		row = self.selected_row
		add = addtrigger.addtrigger( self )
		add.show()
		
	def edit_trigger( self, event=None ):
		row = self.selected_row
		edit = edittrigger.edittrigger( self, row )
		edit.show()

	def delete_trigger( self, event=None ):
		row = self.selected_row
		self.trigger_list.remove( row )
	
	def delete( self, event, data ):
		self.close()
		
	def close( self, *args ):
		self.save_triggers( self.trigger_list )
		self.hide()
		
	def destroy_window( self, data ):
		self.save_triggers( self.trigger_list )
		self.destroy()
			
	def load_triggers( self, clist ):
		trigger_list = self.parent.prefs.get_triggers()
		if len( trigger_list ) == 0:
			clist.append( [ "Drop", "You cannot sell (.*)","regexp", "drop %{P1}"] )
		else:
			for trigger in trigger_list:
				clist.append( trigger )
		
	def save_triggers( self, clist ):
		# FIXME:save triggers
		print "save triggers\n"

	
