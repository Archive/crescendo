from gtk import *
from gnome.ui import *

class stub:
	
	def __init__( self, parent ):
		self.parent = parent

	def def_trigger( self, triggername, pattern, patterntype, texttosend ):
		self.parent.io.input_push( "/def -p1  -F -m" + patterntype + " -aB -t\"" + pattern + "\" " + triggername + " = " + texttosend )

	def undef_trigger( self, triggername ):
		self.parent.io.input_push( "/undef " + triggername )
	
