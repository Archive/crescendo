from gtk import *
from gnome.ui import *

class editworld(GtkDialog):
	def __init__( self, parent, row ):
		GtkDialog.__init__( self )
		self.parent = parent
		self.row = row
		self.set_default_size( 350, 100 )
		self.connect( "destroy", self.close )
		list = parent.world_list

		name = list.get_text( row, 0 )
		world = list.get_text( row, 1 )
		port = list.get_text( row, 2 )
		username = list.get_text( row, 3 )
		password = list.get_text( row, 4 )
		
	
		lblStuff = GtkLabel( "Edit this world" )
		lblStuff.show()
		self.vbox.pack_start( lblStuff, expand=FALSE, fill=FALSE, padding=0 )

		label = GtkLabel( "World Name" )
		label.show()
		self.name_entry = GtkEntry();
		self.name_entry.set_text( name )
		self.name_entry.show()
		hbox = GtkHBox( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=2 )
		hbox.pack_end( self.name_entry, expand=FALSE, fill=FALSE, padding=2 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=FALSE, padding=0 )

		label = GtkLabel( "Address" )
		label.show()
		self.world_entry = GtkEntry();
		self.world_entry.set_text( world )
		self.world_entry.show()
		hbox = GtkHBox ( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=0 )
		hbox.pack_start( self.world_entry, expand=FALSE, fill=FALSE, padding=0 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=TRUE )

		label = GtkLabel( "Port" )
		label.show()
		self.port_entry = GtkEntry();
		self.port_entry.set_text( port )
		self.port_entry.show()
		hbox = GtkHBox( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=0 )
		hbox.pack_start( self.port_entry, expand=FALSE, fill=FALSE, padding=0 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=TRUE )

		label = GtkLabel( "Username" )
		label.show()
		self.username_entry = GtkEntry();
		self.username_entry.set_text( username )
		self.username_entry.show()
		hbox = GtkHBox( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=0 )
		hbox.pack_start( self.username_entry, expand=FALSE, fill=FALSE, padding=0 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=TRUE )

		label = GtkLabel( "Password" )
		label.show()
		self.password_entry = GtkEntry();
		self.password_entry.set_text( password )
		self.password_entry.show()
		hbox = GtkHBox( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=0 )
		hbox.pack_start( self.password_entry, expand=FALSE, fill=FALSE, padding=0 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=FALSE, fill=FALSE )

		self.vbox.show()
		btn = GtkButton( "Save World" )
		btn.connect( "clicked", self.update_row )
		btn.show()
		self.action_area.pack_start( btn, expand=FALSE, fill=FALSE )
		
		btn = GtkButton("Cancel")
		btn.connect( "clicked", self.close )
		btn.show()
		self.action_area.pack_start( btn, expand = FALSE, fill=FALSE )
		
		self.realize()
		
	def update_row (self, *args):
		self.parent.world_list.set_text( self.row, 0, self.name_entry.get_text() )
		self.parent.world_list.set_text( self.row, 1, self.world_entry.get_text() )
		self.parent.world_list.set_text( self.row, 2, self.port_entry.get_text() )
		self.parent.world_list.set_text( self.row, 3, self.username_entry.get_text() )
		self.parent.world_list.set_text( self.row, 4, self.password_entry.get_text() )
		self.close()
		
	def close( self, *args ):
		self.hide()
