from gtk import *
from gnome.ui import *
import sys
import cr_prefs
import connection
import about
import GDK
import world
import trigger
import GtkExtra

#import cr_prefs1
#import cr_prefs

class cr_main(GnomeApp):


	def __init__( self, name, title  ):
		# constructor method for the cr_main object. Subclassed from GnomeApp we
		# pass the name and title to gnomeapp's init. 
		GnomeApp.__init__( self, name, title )

		#figure out what version of python we have, as it is important for the xml parser.
		#perhaps we should pull this out and leave it in the preferences object.
		if( sys.version[0:1] == "1" ):
			self.py_version = "1"
		elif( sys.version[0:1] == "2" ):
			self.py_version = "2"
		self.connect("destroy", self.shutdown_app )
		self.notebook = GtkNotebook()
		self.notebook.set_tab_pos( POS_BOTTOM )
		self.notebook.show()
		self.notebook.connect( "switch-page", self.switch_page_handler )
		vbox = GtkVBox()
		vbox.show()
		
		#menubar = GtkMenuBar()
		menubar = GtkExtra.MenuFactory()
		menubar.show()
		vbox.pack_start( menubar, expand=FALSE )
		
		self.notebook.append_page( vbox, GtkLabel("Status") )
		self.status_output = GtkText()
		self.status_output.show()
		vbox.pack_start( self.status_output, expand=TRUE, fill=TRUE )
		self.set_contents( self.notebook )
		self.set_default_size( 640, 480 )
		self.append_status( "Welcome to Crescendo")
		self.append_status( "Loading preferences and worlds..." )
		# load the prefs module for the correct python version
		self.prefs = cr_prefs.cr_prefs( self )
		self.prefs.load_worlds_file()
		self.prefs.load_triggers_file()
		self.prefs.load_prefs_file()
		self.append_status( "completed." )
		self.append_status( "Loading connection dialog..." )
		self.conn = connection.connection( self  )
		self.trig = trigger.trigger( self )
		self.append_status( "completed." )
		self.append_status( "Creating menus..." )
		
		menubar.add_entries([
			( "Crescendo/Worlds", None, self.conn.show ),
			( "Crescendo/<separator>", None, None ),
			( "Crescendo/Exit", None, self.shutdown_app ),
			( "Help/About", None, None )
		])
		
		self.append_status( "completed." )
		self.show()

	def switch_page_handler( self, notebook, page, page_num  ):
		#	if the default page isn't 0 ie, the status page, we set the default focus to the
		#	gtk input for the world
		if page_num != 0:
			notebook.get_nth_page( page_num ).get_data( 'input-field' ).grab_focus()

	def append_status( self, text ):
		# appends the text arguement to the contents of the status tab.
		self.status_output.insert_defaults( text + "\n" )
				
	def load_world( self, name, connection_string ):
		# creates a new world object and adds a new notebook tab to the cr_main notebook
		# pass the connection_string, ie, world name, and user pass arguements
		self.append_status( "We are connecting to " + name )
		page = world.world( self, name, connection_string )
		page.input.grab_focus()
		self.notebook.append_page( page, GtkLabel( name ) )
	
	def toolbar_info(self ):
		self.toolbar_list = [
			UIINFO_ITEM("New Connection", None, self.conn.show, None ),
			UIINFO_ITEM("Disconnect", None, None, None ),
			UIINFO_ITEM("View Notepad", None, None, None ),
			UIINFO_ITEM("Exit", None, self.shutdown_app, None )
			]
		return self.toolbar_list

	def shutdown_app( self, *args ):
		#we will need to gracefully close the world
		#self.io.input_push("/quit")
		mainquit()	
	
	def menu_info( self ):
		connect_menu_list = [
			UIINFO_ITEM("World Manager", None, self.conn.show, None ),
			UIINFO_ITEM("Exit", None, self.shutdown_app, None )
			]
		tools_menu_list = [
			UIINFO_ITEM("Triggers", None, self.trig.show, None )
			]
		help_menu_list = [
			UIINFO_ITEM("About", None, about.about, None )
			]
		menu_list = [
			UIINFO_SUBTREE("Crescendo", connect_menu_list ),
			UIINFO_SUBTREE("Tools", tools_menu_list ),
			UIINFO_SUBTREE("Help", help_menu_list )
			]
		return menu_list

	def get_current_world( self ):
		pagenumber = self.notebook.get_current_page()
		if (pagenumber == 0):
			return 0
		else:
			world = self.notebook.get_nth_page( pagenumber )
			return world;

	def show_trigger_dialog( self ):   	   
			currentworld = self.get_current_world()
			print repr(currentworld)
			if (currentworld != 0):  # don't run unless there is a current world
				print repr(currentworld)
				currentworld.get_data( 'trigger' ).show()
