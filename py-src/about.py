from gnome.ui import *
class about(GnomeAbout):

	name = "Crescendo"
	version = "2.0-prealpha"
	copyright = "(c) 1999, 2000, 2001, 2002 Will LaShell"
	desc = "Crescendo(c) is a gui front end to Tiny Fugue, Visit the Crescendo team at http://crescendo.lyrical.net"
	text = [ 
		"Will LaShell <will@lashell.net>",
		"Joey Wilhelm <tarken@lyrical.net>",
		"David Winkler (retired)",
		]
	def __init__( self, data ):
		GnomeAbout.__init__( self, self.name, self.version, self.copyright, self.text, self.desc )
		self.show()
		