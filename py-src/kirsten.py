from gtk import *
import re
import filter
import gtk
import os


class kirsten:


	def __init__( self, parent ):
		self.parent = parent
		self.io = self.parent.io
		
		self.states = ['any']  
		self.filters = {}

		self.default_filters()

	def default_filters( self ):
		self.define_filter( name='to_cr', state='any', priority=8, match='tf', regexp='(.*)', actions=[['to_cr', "$&"]])
		self.define_filter( name='to_tf', state='any', priority=8, match='cr', regexp='(.*)', actions=[['to_tf', "$&"]])
		self.define_filter( name='quit' , state='any', priority=8, match='cr', regexp='^/quit', actions=[['quit']])


	def from_cr ( self, text ):
		we_matched_a_filter = None # False
		for priority in range( 1, 9 ):
			if ( we_matched_a_filter ): # more than 1 filter can be matched as long as they all have the same priority
				return()
			
			for f in self.filters.values():
				if ( ( f.match == "cr" ) and ( f.priority == priority ) and ( f.state in self.states )):
					matchobj = re.search( f.regexp, text )
					if ( matchobj != None ):
						self.do_actions( matchobj=matchobj, string=text, actions=f.actions )
						we_matched_a_filter = 1 # True
	
								   		
	def from_tf ( self, text ):
		we_matched_a_filter = None # False
		for priority in range ( 1, 9 ):
			if ( we_matched_a_filter ): # more than 1 filter can be matched as long as they all have the same priority
				return()
			
			for f in self.filters.values():
				if ( ( f.match == "tf" ) and ( f.priority == priority ) and ( f.state in self.states ) ):
					matchobj = re.search( f.regexp, text )
					if ( matchobj != None ):
						self.do_actions( matchobj=matchobj, string=text, actions=f.actions )
						we_matched_a_filter = 1 # True
						

	def do_actions ( self, matchobj=None, string="", actions=[] ):
		for action in actions:
			args  = self.expand_args( matchobj, string, action[1:] )
			name = action[0]
			if ( name == 'define_filter' ):
				self.define_filter( args, matchobj, string )
			if ( name == 'send' ):
				self.send( args, matchobj, string )
			if ( name == 'to_tf' ):
				self.to_tf( args, matchobj, string )
			if ( name == 'to_cr' ):
				self.to_cr( args, matchobj, string )
			if ( name == 'quit' ):
				self.quit( args, matchobj, string )
			if ( name == 'enter_state' ):
				self.enter_state( args, matchobj, string )


	def expand_args ( self, matchobj=None, string="", arglist=[] ):
		returnlist = []
		for arg in arglist:
			#split each arg at $ and replace each $expression seperately then concatenate all the parts back together
			#this is to avoid $1 being replaced by '$2' (which is ok) but it is not ok if that $2 is then replaced by
			#the value of $2 on the second pass
			#this will allow the groups that replace $expressions to themselves contain things that look like
			#$expressions that will not be replaced.

			parts  = arg.split("$")
			parts_done = ""
			for part in parts[1:]:
				part = '$%s' % (part)
				groupnum = 1
				groups = matchobj.groups()
				for group in groups:
					pattern = '$%d' % ( groupnum )
					if ( part.rfind( pattern ) != -1 ):
						groupnum = groupnum + 1
						part = part.replace( pattern, group )
						break
				else:
					if ( part.rfind( "$_") != -1 ):
						part = part.replace( "$_", matchobj.group(0) ) # the part of the string that was matched by the regexp
					elif ( part.rfind ( "$&" ) != -1 ):
						part = part.replace( "$&", string ) # the whole line

				parts_done = parts_done + part 
			returnlist.append( parts_done )
		return returnlist	
			
					
	# Action functions from here on down


	def define_filter( self, name=None, state='any', priority=5, match='tf', regexp=None, actions=[] ):
		if ( self.filters.has_key( name ) ):
			del self.filters[ name ]
		
		newfilter = filter.filter( name, state, priority, match, regexp, actions )
		self.filters[ newfilter.name ] = newfilter
		self.to_cr( ["xfilter defined"] )
		
		
	def send ( self, arglist=[] , matchobj=None, string=None ):
		if ( arglist == [] ):
			return
		if ( ( arglist[0] == "tf" ) or ( arglist[0] == "tinyfugue" ) or ( arglist[0] == "mud" ) ):
			# fixme: first test to make sure arglist[1] is a string
			self.to_tf( arglist[1] )
		elif ( (arglist[0] == "cr") or ( arglist[0] == "crescendo" ) or ( arglist[0] == "client" ) ):
			# fixme: first test to make sure arglist[1] is a string 
			self.to_tf( arglist[1] )
			
	def to_tf ( self, arglist=[], matchobj=None, string=None ):
		self.io.input_push( arglist[0] )

	def to_cr ( self, arglist=[], matchobj=None, string=None ):
		self.parent.output.feed( arglist[0] )
		
	def quit ( self, arglist=[], matchobj=None, string=None ):
		self.io.kill_me()

		
		
	
		
		
		
