from gtk import *
from gnome.ui import *
import GtkExtra

import editworld
import addworld

class connection(GtkWindow):
	def __init__( self, parent ):
		GtkWindow.__init__( self, WINDOW_DIALOG )
		self.parent = parent
		self.selected_row = None
		self.selected_mrow = None
		self.set_default_size( 550, 400 )
		self.connect("destroy", self.close )
		
		self.notebook = GtkNotebook()
		self.notebook.show()
		self.add( self.notebook )
		self.notebook.connect( "switch-page", self.switch_page_handler )
		
		self.world_vbox = GtkVBox()
		self.world_vbox.show()
		
		self.mudconnector_vbox = GtkVBox()
		self.mudconnector_vbox.show()
		
		self.notebook.append_page( self.world_vbox, GtkLabel( "Your Worlds" ) ) 
		
		# code for the worlds page set by users
		titles = ["World Name", "Address", "Port", "Username", "Password" ]
		self.world_list = GtkCList( 5, titles )
		self.world_list.set_column_width( 0, 150 )
		self.world_list.set_column_width( 1, 150 )
		self.world_list.set_column_width( 2, 30 )
		self.load_worlds( self.world_list )
		self.world_list.connect( "select_row", self.select_row )
		scroll_win = GtkScrolledWindow()
		scroll_win.show()
		scroll_win.add( self.world_list )
		scroll_win.set_policy( POLICY_NEVER, POLICY_AUTOMATIC )
		self.world_list.show()
		self.world_vbox.pack_start( scroll_win, expand=TRUE, fill=TRUE )

		#buttons
		self.world_vbox.pack_start( GtkHSeparator(), expand=TRUE )
		
		hbox = GtkHButtonBox()
		hbox.set_spacing( 25 )
		hbox.set_border_width( 10 )
		hbox.set_layout_default( BUTTONBOX_SPREAD )
		hbox.show()
		self.world_vbox.pack_start( hbox, expand=FALSE )
		
		btn = GtkButton("Launch World")
		btn.connect( "clicked", self.click_launch )
		btn.show()
		hbox.pack_start( btn )
		btn = GtkButton("Edit")
		btn.connect( "clicked", self.edit_world )
		btn.show()
		hbox.pack_start(btn)
		btn=GtkButton("Add New")
		btn.connect( "clicked", self.add_world )
		btn.show()
		hbox.pack_start( btn )
		btn = GtkButton("Cancel")
		btn.connect( "clicked", self.close )
		btn.show()
		hbox.pack_start( btn )
		
		self.notebook.append_page( self.mudconnector_vbox, GtkLabel( "MudConnector Worlds" ) )
		# code for the mud connector page
		hbox = GtkHBox()
		hbox.show()
		self.mudconnector_vbox.pack_start( hbox, expand=FALSE, fill=TRUE )
		
		frame = GtkFrame( "Filter World List" )
		frame.show()
		hbox.pack_start( frame, expand=TRUE, fill=TRUE )
		
		table = GtkTable( 3, 4, homogeneous=FALSE )
		table.show()
		frame.add( table )
		
		label = GtkLabel( "Genre" )
		label.show()
		table.attach( label, 0, 1, 0, 1 )
	
		menu = GtkExtra.MenuFactory( type=GtkExtra.MENU_FACTORY_OPTION_MENU )
		menu.add_entries([
			( "All", None, None, None ),
			( "LPmud", None, None, None ),
			( "Moo", None, None, None ),
			( "Mush", None, None, None )
		])
		#menu.set_usize( 25, -1 )		
		menu.show()
		table.attach( menu, 1, 2, 0, 1 )

		btn = GtkButton( "Apply" )
		btn.show()
		table.attach( btn, 2, 3, 2, 3, xpadding=3, ypadding=3 )
		
		hbox = GtkHBox()
		hbox.show()
		self.mudconnector_vbox.pack_start( hbox, expand=TRUE, fill=TRUE )
		
		titles = [ "World Name" ]	
		self.mud_world_clist = GtkCList( 1, titles )
		#self.mud_world_clist.set_column_width( 0, 50 )
		scroll_win = GtkScrolledWindow()
		scroll_win.show()
		scroll_win.add( self.mud_world_clist )
		scroll_win.set_policy( POLICY_NEVER, POLICY_AUTOMATIC )
		#scroll_win.set_usize( 50, -1 )
		self.mud_world_clist.show()
		hbox.pack_start( scroll_win, expand=FALSE, fill=TRUE )

		vbox = GtkVBox()
		vbox.show()
		hbox.pack_start( vbox, expand=TRUE, fill=TRUE )
		
		label = GtkLabel( "FOO" )
		label.show()
		vbox.pack_start( label )
		
		self.mudconnector_vbox.pack_start( GtkHSeparator(), expand=TRUE )
		
		hbox = GtkHButtonBox()
		hbox.set_spacing( 25 )
		hbox.set_border_width( 10 )
		hbox.set_layout_default( BUTTONBOX_SPREAD )
		hbox.show()
		self.mudconnector_vbox.pack_start( hbox, expand=FALSE )
		
		btn = GtkButton("Launch World")
		btn.connect( "clicked", self.click_mlaunch )
		btn.show()
		hbox.pack_start( btn )
		btn = GtkButton("Update World List")
		btn.connect( "clicked", self.update_world_list )
		btn.show()
		hbox.pack_start( btn )
		btn = GtkButton("Cancel")
		btn.connect( "clicked", self.close )
		btn.show()
		hbox.pack_start( btn )
		self.realize()
	
	def switch_page_handler( self, notebook, page, page_num  ):
		print "page" + str( page )

	def click_launch( self, row=None ):
		row = self.selected_row
		self.launch_world( row )
		
	def click_mlaunch( self, row=None ):
		row = self.selected_mrow
		self.launch_mudconnector_world( row )

	def select_row( self, clist, row, col, event ):
		print " row -> " + str( row ) + " self.sel_row -> " + str( self.selected_row )
		if row == self.selected_row:
			self.launch_world( row )
		else:
			self.selected_row = row

	def launch_world( self, row=None ):
		print " launchrow -> " + str( row ) + " self.sel_row -> " + str( self.selected_row )
		if row == None:
			row = self.selected_row
		name = self.world_list.get_text( row, 0 )
		world = self.world_list.get_text( row, 1 )
		port = self.world_list.get_text( row, 2 )
		self.parent.load_world( name, world + " " + port )
		self.hide()
	
	def launch_mudconnector_world( self, row=None ):
		print " launchrow -> " + str( row ) + " self.sel_row -> " + str( self.selected_row )
		if row == None:
			row = self.selected_row
		name = self.world_list.get_text( row, 0 )
		world = self.world_list.get_text( row, 1 )
		port = self.world_list.get_text( row, 2 )
		self.parent.load_world( name, world + " " + port )
		self.hide()
		
	def update_world_list( self ):
		pass
	
	def delete( self, event, data ):
		self.close()
		
	def close( self, *args ):
		self.save_worlds( self.world_list )
		self.hide()
		
	def destroy_window( self, data ):
		self.save_worlds( self.world_list )
		self.destroy()
			
	def load_worlds( self, clist ):
		world_list = self.parent.prefs.get_worlds()
		if len( world_list ) == 0:
			clist.append( [ "Darkland", "darkland.simrin.net", "3000", "", "" ] )
		else:
			for world in world_list:
				clist.append( world )
		
	def save_worlds( self, clist ):
		# save worlds
		print "save worlds\n"

	def edit_world( self, event=None ):
		row = self.selected_row
		edit = editworld.editworld( self, row )
		edit.show()

	def add_world( self, event=None ):
		row = self.selected_row
		add = addworld.addworld( self )
		add.show()
