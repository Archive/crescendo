import os
import popen2
import fcntl 
import FCNTL
import re
import gtk
import GDK

class cr_io:

	FROM_CHANGED = 0x02
	FROM_RETURN = 0x01

	history = []
	history_index = 0
	# we need a zvt widget, and an input widget, we got the rest
	def __init__( self, parent, zvt, entry ):
		self.parent = parent
		self.zvt = zvt
		self.entry = entry
		self.kirsten = self.parent.kirsten
		self.poutput,self.pinput = popen2.popen2( "tf" )
		fcntl.fcntl( self.poutput.fileno(), FCNTL.F_SETFL, os.O_NDELAY )
		fcntl.fcntl( self.pinput.fileno(), FCNTL.F_SETFL, os.O_NDELAY )
		self.inhandle = gtk.input_add( self.poutput, GDK.INPUT_READ, self.output_read  )
		self.input_push( "/visual off")
		
	def kill_me( self ):
		self.pinput.write( "/quit\n" )
		# This input_remove() kills the problems we were having with a broken I/O pipe
		gtk.input_remove( self.inhandle )
		self.pinput.close()
		self.poutput.close()
		self.parent.destroy()
	
	def input_push( self, cmd ):
		self.pinput.write( cmd + "\n")
		self.pinput.flush()

	def input_read( self, data, type ):
		#kirsten now handles "/quit"
		if( type != self.FROM_RETURN ):
			return
		cmd_text = self.entry.get_chars( 0, -1 )
		self.kirsten.from_cr( cmd_text )
		self.entry.set_text( "" )
		self.history_append( cmd_text )
		
	def output_read( self, output, data ):
		source_text = output.read()
		if( len( source_text ) == 0 ):
			gtk.input_remove( self.inhandle )
		else:
			output_text = source_text
			output_text = re.sub( "\n", "\r\n", source_text )
			# split chucks the delimiters - so the for loop puts them back in
			outputlist = output_text.split("\n")
			for text in outputlist[:-1]:
				if (text !=""):
					text = text + "\n"
					self.kirsten.from_tf( text )
			if ( outputlist[-1] != "" ):
				self.kirsten.from_tf( outputlist[-1] )		

	def history_append( self, cmd ):
		self.history = self.history + [ cmd ]
		self.history_index = 0
	
	def history_recall_up( self ):
		if ( abs(self.history_index) >= len( self.history ) ):
			 self.history_index = 0
		else:	 
			self.history_index = self.history_index - 1
		self.entry.set_text( self.history[ self.history_index ] )
		self.entry.select_region( 0, -1 )
	
	def history_recall_down( self ):
		if( self.history_index == 0):
			self.history_index = 0 - len( self.history )
		else:
			self.history_index = self.history_index + 1 
		self.entry.set_text( self.history[ self.history_index ] )
		self.entry.select_region( 0, -1 )		
		
