from xml.sax import saxutils
from xml.sax import saxlib
from xml.sax import saxexts
import string
import os

#from xml.sax import feature_namespaces

class cr_pref_handler( saxlib.DocumentHandler ):
	def __init__( self, parent):
		# invoking the handler class with a reference to the parent object
		# we need this to let handler add worlds to the list
		self.parent = parent
		
	def startElement( self, name, attrs ):
		if name == "world":
			world_list = [ attrs.getValue( "name" ), attrs.getValue( "address" ), attrs.getValue( "port" ), attrs.getValue( "username" ), attrs.getValue( "password" ) ]	
			self.parent.worlds.append( world_list )		
		elif name == "triggers":
			trigger_list = [ attrs.getValue( "triggername" ), attrs.getValue( "matchpattern" ),
							 attrs.getValue( "patterntype" ), attrs.getValue( "texttosend" ) ]
			self.parent.triggers.append( trigger_list )

class cr_prefs:
	def __init__(self, parent):

		self.parent = parent
		self.worlds = []
		self.parser = saxexts.make_parser()
		handler = cr_pref_handler( self )
		self.parser.setDocumentHandler( handler )

	def load_worlds_file( self ):
		worlds_file = os.environ["HOME"] + "/.crescendo/worlds.xml"
		self.worlds = []
		if os.path.isfile( worlds_file ):
			self.parent.append_status( "Located worlds file, parsing now." )
			self.parser.parseFile( open( worlds_file ) )
			self.parser.close()
		else:
			self.parent.append_status( "No worlds file, loading defaults." )
	
	def get_worlds( self ):
		# return this objects worlds list
		return self.worlds

	def load_triggers_file( self ):
		triggers_file = os.environ["HOME"] + "/.crescendo/triggers.xml"
		self.triggers = []
		if os.path.isfile( triggers_file ):
			self.parent.append_status( "Located triggers file, parsing now." )
			self.parser.parse( triggers_file )
			self.parser.close()
		else:
			self.parent.append_status( "No triggers file, loading defaults." )

	def get_triggers(self):
		# return this object's triggers list
		return self.triggers
	
