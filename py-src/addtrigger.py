from gtk import *
from gnome.ui import *

class addtrigger(GtkDialog):
	def __init__( self, parent ):
		GtkDialog.__init__( self )
		self.parent = parent
		self.set_default_size( 350, 100 )
		self.connect( "destroy", self.close )
		list = parent.trigger_list

		triggername = ""
		matchpattern = ""
		patterntype = "regexp"
		texttosend = ""
		
		lblStuff = GtkLabel( "Add new trigger" )
		lblStuff.show()
		self.vbox.pack_start( lblStuff, expand=FALSE, fill=FALSE, padding=0 )

		label = GtkLabel( "Trigger name" )
		label.show()
		self.triggername_entry = GtkEntry();
		self.triggername_entry.set_text( triggername )
		self.triggername_entry.show()
		hbox = GtkHBox( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=2 )
		hbox.pack_end( self.triggername_entry, expand=FALSE, fill=FALSE, padding=2 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=FALSE, padding=0 )

		label = GtkLabel( "Pattern to match" )
		label.show()
		self.matchpattern_entry = GtkEntry();
		self.matchpattern_entry.set_text( matchpattern )
		self.matchpattern_entry.show()
		hbox = GtkHBox ( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=0 )
		hbox.pack_start( self.matchpattern_entry, expand=FALSE, fill=FALSE, padding=0 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=TRUE )

		label = GtkLabel( "Pattern type" )
		label.show()
		self.radiogroup = GtkContainer()
		self.regexp = GtkRadioButton( label="Regexp")
		self.regexp.show()
		self.glob = GtkRadioButton( label="Glob", group=self.regexp )
		self.glob.show()
		if ( patterntype == "regexp" ):
			self.regexp.set_active( active=TRUE )
		else :
			self.glob.set_active( active=TRUE )
		hbox = GtkHBox( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=4 )
		hbox.pack_start( self.regexp, expand=FALSE, fill=FALSE, padding=4 )
		hbox.pack_start( self.glob, expand=TRUE, fill=TRUE, padding=4 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=TRUE )
		
		label = GtkLabel( "Text to send to mud" )
		label.show()
		self.texttosend_entry = GtkEntry();
		self.texttosend_entry.set_text( texttosend )
		self.texttosend_entry.show()
		hbox = GtkHBox( homogeneous=TRUE, spacing=0 )
		hbox.pack_start( label, expand=FALSE, fill=FALSE, padding=0 )
		hbox.pack_start( self.texttosend_entry, expand=FALSE, fill=FALSE, padding=0 )
		hbox.show()
		self.vbox.pack_start( hbox, expand=TRUE, fill=TRUE )

		self.vbox.show()
		btn = GtkButton( "Save Trigger" )
		btn.connect( "clicked", self.update_row )
		btn.show()
		self.action_area.pack_start( btn, expand=FALSE, fill=FALSE )
		
		btn = GtkButton("Cancel")
		btn.connect( "clicked", self.close )
		btn.show()
		self.action_area.pack_start( btn, expand = FALSE, fill=FALSE )
		
		self.realize()
		
	def update_row (self, *args):
		triggername = self.triggername_entry.get_text()
		matchpattern = self.matchpattern_entry.get_text()
		if ( self.regexp.get_active() ):
			patterntype = "regexp"
		else :
			patterntype = "glob"
		texttosend = self.texttosend_entry.get_text()	
		newrow = [
			triggername,
			matchpattern,
			patterntype,
			texttosend
			]
		self.parent.trigger_list.append( newrow )
		self.close()
		
	def close( self, *args ):
		self.hide()
