from gtk import *
from gnome.ui import *
from gnome.zvt import *
import cr_io
import kirsten
import GDK
import temp
import GtkExtra
import trigger

class world(GtkVBox):
#	color_list = [ 
#				( 0x0000,0x0000,0x0000 ),
#				( 0xaaaa,0x0000,0x0000 ),
#				( 0x0000,0x0000,0xaaaa ),
#				( 0xaaaa,0x0000,0x5555 ),
#				( 0x0000,0xaaaa,0x0000 ),
#				( 0xaaaa,0xaaaa,0x0000 ),
#				( 0x0000,0xaaaa,0xaaaa ),
#				( 0xaaaa,0xaaaa,0xaaaa ),
#				( 0x5555,0x5555,0x5555 ),
#				( 0xffff,0x5555,0x5555 ),
#				( 0x5555,0x5555,0xffff ),
#				( 0xffff,0x5555,0xffff ),
#				( 0x5555,0xffff,0x5555 ),
#				( 0xffff,0xffff,0x5555 ),
#				( 0x5555,0xffff,0xffff ),
#				( 0xffff,0xffff,0xffff ),
#				( 0xfafa,0xd7d7,0xebeb ),
#				( 0x0000,0x0000,0x0000 ),
#				]
# the following is necessary only until jamesh fixes pygnome
	color_list = [ 
				( 0x0000,0x0000,0x0000 ),
				( -21846,0x0000,0x0000 ),
				( 0x0000,0x0000,-21846 ),
				( -21846,0x0000,0x5555 ),
				( 0x0000,-21846,0x0000 ),
				( -21846,-21846,0x0000 ),
				( 0x0000,-21846,-21846 ),
				( -21846,-21846,-21846 ),
				( 0x5555,0x5555,0x5555 ),
				( -1,0x5555,0x5555 ),
				( 0x5555,0x5555,-1 ),
				( -1,0x5555,-1 ),
				( 0x5555,-1,0x5555 ),
				( -1,-1,0x5555 ),
				( 0x5555,-1,-1 ),
				( -1,-1,-1 ),
				( -1286,-10281,-5141 ),
				( 0x0000,0x0000,0x0000 ),
				]
				
	FROM_CHANGED = 0x02
	FROM_RETURN = 0x01
	
	def __init__( self, parent, name, connection_string ):
		GtkVBox.__init__( self )
		self.parent = parent
		self.name = name
		self.connection_string = connection_string
		#self.trig = trigger.trigger( self )
		self.stub = temp.stub( self )
		self.set_data( 'stub', self.stub )
		
		self.parent.connect("key_press_event", self.key_press_handler )
		
		menubar = GtkExtra.MenuFactory()
		menubar.show()
		self.pack_start( menubar, expand=FALSE )
		
		hbox = GtkHBox()
		hbox.show()
		self.pack_start(hbox, expand=TRUE, fill=TRUE )
				
		self.output = ZvtTerm()
		self.output.show()
		self.output.set_color_scheme( self.color_list )
		if self.parent.prefs.prefs.has_key( "scrollback_lines" ):
			self.output.set_scrollback( int( self.parent.prefs.prefs["scrollback_lines"] ) )
		if self.parent.prefs.prefs.has_key( "visual_bell" ):
			self.output.set_bell( int( self.parent.prefs.prefs["visual_bell"] ) )
		self.output.unset_flags( CAN_FOCUS )
		self.output.set_blink( FALSE )

		hbox.pack_start( self.output, expand=TRUE, fill=TRUE )
		scrollbar = GtkVScrollbar( self.output.adjustment)
		scrollbar.unset_flags( CAN_FOCUS )
		scrollbar.show()
		hbox.pack_start( scrollbar, expand=FALSE)
		
		self.input = GtkEntry()
		self.input.show()
		#FIXME:
		self.set_data( 'input-field', self.input )
		
		hbox = GtkHBox()
		hbox.show()
		hbox.pack_start( self.input, expand=TRUE, fill=TRUE )
		btn = GtkButton("Clear")
		btn.show()
		btn.connect( "clicked", self.clear_input )
		hbox.pack_start( btn, expand=FALSE )
		self.pack_start( hbox, expand=FALSE )		

		self.io = None
		self.kirsten = kirsten.kirsten( self )
		self.io = cr_io.cr_io( self, self.output, self.input )
		self.kirsten.io = self.io # don't ask
		
		self.input.connect( "activate", self.io.input_read, self.FROM_RETURN )
		self.input.connect( "changed", self.io.input_read, self.FROM_CHANGED  )
		
		menubar.add_entries([
			( "Crescendo/Worlds", None, self.parent.conn.show ),
			( "Crescendo/<separator>", None, None ),
			( "Crescendo/Exit", None, self.parent.shutdown_app ),
			( "World/Reconnect", None, self.reconnect ),
			( "Tools/Triggers", None, self.parent.trig.show ),
			( "Help/About", None, None )
		])
		
		self.show()
		self.io.input_push("/addworld " + self.name + " " + connection_string )
		self.io.input_push("/connect " + self.name )

	def world_menu_info( self ):
		#connect_menu_list = [
		#	UIINFO_ITEM("", None, self.conn.show, None ),
		#	UIINFO_ITEM("Exit", None, self.shutdown_app, None )
		#	]
		#
		#help_menu_list = [
		#	UIINFO_ITEM("About", None, about.about, None )
		#	]
		menu_list = [
		#	UIINFO_SUBTREE("Crescendo", connect_menu_list ),
		#	UIINFO_SUBTREE("Help", help_menu_list )
			UIINFO_ITEM( "Close", None, None, None )
		]
		return menu_list
	def key_press_handler( self, data, event ):
		keyval = event.keyval
		if( keyval == GDK.Page_Up or keyval == GDK.KP_Page_Up ):
			print "page up\n"
			self.parent.emit_stop_by_name( "key_press_event" )
		elif( keyval == GDK.Page_Down or keyval == GDK.KP_Page_Down ):
			print "page down\n"
			self.parent.emit_stop_by_name( "key_press_event" )
		elif( keyval == GDK.Up ):
			self.io.history_recall_up()
			self.parent.emit_stop_by_name( "key_press_event" )
		elif( keyval == GDK.Down ):
			self.io.history_recall_down()
			self.parent.emit_stop_by_name( "key_press_event" )
	
	def clear_input( self, data ):
		self.input.set_text( "" )


	def reconnect( self, data ): 
		self.io.input_push("/connect " + self.name )
