#!/usr/bin/python2

import cr_main
from gtk import *
from gnome.ui import *
import GDK

import os, string, sys
from os import path
import fcntl, FCNTL

# credits go to Kevin Turner <acapnotic@foobox.net> for the very excellent  exists in path function
# without it, there would be much pain in making crescendo run. 

PAGER = None

def exists_in_path(filename, search_path=None):
    #"""Does the file exist in the given search path?

    #If so, return the absolute path.  If not, return False.
    #search_path defaults to $PATH.
    #"""
	if path.isabs(filename):
		return (path.isfile(filename) and filename)

	if search_path is None:
		search_path = os.environ["PATH"]

	directories = string.split(search_path, ":")

	for d in directories:
		found = path.isfile(path.join(d, filename))
		if found:
			return path.join(d, filename)

	return None

if exists_in_path( "tf" ) == None:
	err = GnomeErrorDialog( "Could Not Find Tinyfugue!\nPlease download and install it from http://crescendo.lyrical.net\nor the TinyFugue main site." )
	err.connect( "clicked", mainquit )
	
else:

	win = cr_main.cr_main("crescendo", "Crescendo" )

mainloop()	
