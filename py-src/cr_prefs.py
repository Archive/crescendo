from xml.sax import ContentHandler
from xml.sax import make_parser
from xml.sax.handler import feature_namespaces
import string
import os

#from xml.sax import feature_namespaces

class cr_pref_handler( ContentHandler ):
	def __init__( self, parent):
		# invoking the handler class with a reference to the parent object
		# we need this to let handler add worlds to the list
		self.parent = parent
		self.inworld_content = 0
		self.intrigger_content = 0
		self.inprefs = 0
		self.build_world = {}
		self.build_trigger = {}
		self.cur_element = ""
		self.cur_element_content = 0
		
	def startElement( self, name, attrs ):
		if name == "world":
			# if its a name, we initialize the content to 1 and prepare an empty list to append world data onto
			self.inworld_content = 1

		elif name == "trigger":
			# if its a name, we initialize the content to 1 and prepare and empty list to append trigger data onto
			self.intrigger_content = 1	

		elif name == "crescendo_prefs":
			self.inprefs = 1

		elif self.inworld_content or self.intrigger_content or self.inprefs:
			self.cur_element = name
			self.cur_element_content = 1
	
	def characters( self, char ):
		if self.cur_element_content == 1:
			if self.inworld_content:
				if self.build_world.has_key( self.cur_element ):
					self.build_world[ self.cur_element ] = self.build_world[ self.cur_element ] + char
				else:
					self.build_world[ self.cur_element ] = char
			elif self.intrigger_content:
				if self.build_trigger.has_key( self.cur_element ):
					self.build_trigger[ self.cur_element ] = self.build_trigger[ self.cur_element ] + char
				else:
					self.build_trigger[ self.cur_element ] = char
			elif self.inprefs:
				if self.parent.prefs.has_key( self.cur_element ):
					self.parent.prefs[ self.cur_element ] = self.parent.prefs[ self.cur_element ] + char
				else:
					self.parent.prefs[ self.cur_element ] = char
					
	def endElement( self, name ):
		if name == "world":
			#set up the list check for the element, if it exists, add it in.
			if self.build_world.has_key( "name" ):
				w_name = self.build_world["name"]
			else:
				w_name = ""
			if self.build_world.has_key( "address" ):
				w_address = self.build_world["address"]
			else:
				w_address = ""
			if self.build_world.has_key( "port" ):
				w_port = self.build_world["port"]
			else:
				w_port = ""
			if self.build_world.has_key( "username" ):
				w_username = self.build_world["username"]
			else:
				w_username = ""
			if self.build_world.has_key( "password" ):
				w_password = self.build_world["password"]
			else:
				w_password = ""
				
			world_list = [ str( w_name ), str( w_address ), str( w_port ), str( w_username ), str( w_password ) ]
			self.parent.worlds.append( world_list )
			self.inworld_content = 0
			self.build_world = {}

		elif name == "trigger":
			# set up the list check for the element, if it exists, add it in.
			if self.build_trigger.has_key( "name" ):
				t_name = self.build_trigger["name"]
			else:
				t_name = ""
			if self.build_trigger.has_key( "pattern" ):
				t_pattern = self.build_trigger["pattern"]
			else:
				t_pattern = ""
			if self.build_trigger.has_key( "patterntype" ):
				t_patterntype = self.build_trigger["patterntype"]
			else:
				t_patterntype = ""
			if self.build_trigger.has_key( "sendtext" ):
				t_sendtext = self.build_trigger["sendtext"]
			else:
				t_sendtext = ""

			trigger_list = [ str( t_name ), str( t_pattern ), str( t_patterntype ), str( t_sendtext ) ]
			self.parent.triggers.append( trigger_list )
			self.intrigger_content = 0
			self.build_trigger = {}
			
		elif name == "crescendo_prefs":
			self.inprefs = 0
		
		self.cur_element = ""
		self.cur_element_content = 0	

class cr_prefs:
	def __init__(self, parent):

		self.parent = parent
		self.worlds = []
		self.prefs = {}
		self.triggers = []
		self.parser = make_parser()
		self.parser.setFeature(feature_namespaces, 0)
		handler = cr_pref_handler( self )
		self.parser.setContentHandler( handler )
		
	def load_worlds_file( self ):
		worlds_file = os.environ["HOME"] + "/.crescendo/worlds.xml"
		self.worlds = []
		if os.path.isfile( worlds_file ):
			self.parent.append_status( "Located worlds file, parsing now." )
			self.parser.parse( worlds_file )
		else:
			self.parent.append_status( "No worlds file, loading defaults." )

	def load_prefs_file( self ):
		prefs_file = os.environ["HOME"] + "/.crescendo/prefs.xml"
		if os.path.isfile( prefs_file ):
			self.parent.append_status( "Located prefs file, parsing now." )
			self.parser.parse( prefs_file )
		else:
			self.parent.append_status( "No worlds file, loading defaults." )

	def load_triggers_file( self ):
		triggers_file = os.environ["HOME"] + "/.crescendo/triggers.xml"
		self.triggers = []
		if os.path.isfile( triggers_file ):
			self.parent.append_status( "Located triggers file, parsing now." )
			self.parser.parse( triggers_file )
		else:
			self.parent.append_status( "No triggers file, loading defaults." )
		
	
	def get_worlds( self ):
		# return this objects worlds list
		return self.worlds
		
	def get_prefs( self ):
		return self.prefs

	def get_triggers( self ):
		return self.triggers
